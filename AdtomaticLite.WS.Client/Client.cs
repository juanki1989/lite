﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AdtomaticLite.WS.Client
{
    public class Client
    {
        public static bool IsClienteActivo(int idCliente)
        {
            try
            {
                AdtomaticWS.WebServiceINSLitePortTypeClient client = new AdtomaticWS.WebServiceINSLitePortTypeClient();
                return client.IsClienteActivo(idCliente);
            }
            catch (Exception ex)
            {
                throw new Exception("Ocurrio un error al intentar conectarse al servidor:" + ex.Message);
            }
        }

        public static List<Entities.Anuncio> GetAnunciosActivos(int idCliente)
        {
            try
            {
                AdtomaticWS.WebServiceINSLitePortTypeClient client = new AdtomaticWS.WebServiceINSLitePortTypeClient();
                var renovaciones = client.GetAnunciosActivos(idCliente);
                List<Entities.Anuncio> anuncios = new List<Entities.Anuncio>();

                List<int> horasDeSalida = new List<int>();
                //List<> _anunciosList = _anunciosList.OrderBy(anuncio => anuncio.NextRun).ToList();
                foreach (AdtomaticWS.Renovacion renov in renovaciones)
                {
                    Entities.Anuncio anuncio = new Entities.Anuncio();
                    anuncio.Name = renov.Name;
                    anuncio.Mail = renov.Mail;
                    anuncio.Password = renov.Password;
                    anuncio.Tipo = GetTablonEnum(renov.Tipo);

                    anuncio.IdRunType = 1;
                    anuncio.StartDate = renov.StartDate;
                    anuncio.StartTime = renov.StartTime;
                    anuncio.EndDate = renov.EndDate;
                    anuncio.EndTime = renov.EndTime;
                    if (anuncio.Tipo == Entities.Enum.eTablones.MilAnunciosRenovPago)
                        anuncio.EndTime = null;
                    anuncio.TimeInterval = renov.TimeInterval;
                    anuncio.RunEveryDay = renov.RunEveryDay;
                    anuncio.RunWeekDays = renov.RunWeekDays;
                    anuncio.RunEveryXDays = renov.RunEveryXDays;
                    anuncio.IntervaloRenovPago = renov.IntervaloRenovPago;
                    anuncio.RenovarNocturno = renov.RenovarNocturno;
                    if (anuncio.Tipo == Entities.Enum.eTablones.MilAnunciosRenovPago)
                        anuncio.NextRun = anuncio.CalculateNextRunDate();
                    
                    //comentado xq ya no se ejecutara en cuanto se inicie el lite
                    if (anuncio.Tipo == Entities.Enum.eTablones.MilAnunciosRenovPago && anuncio.IntervaloRenovPago != 0)
                        anuncio.NextRun = anuncio.StartTime;//new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, DateTime.Now.Hour, DateTime.Now.Minute, 0).AddMinutes(2);                       
                    
                    anuncios.Add(anuncio);
                }
                
                return anuncios;
            }
            catch (Exception ex)
            {
                throw new Exception("Ocurrio un error al intentar conectarse al servidor:" + ex.Message);
            } 
        }
        
        public static bool EnviarMail(int idCliente, string nombreAnuncio)
        {
            try
            {
                AdtomaticWS.WebServiceINSLitePortTypeClient client = new AdtomaticWS.WebServiceINSLitePortTypeClient();
                return client.EnviarMail(idCliente, nombreAnuncio);
            }
            catch (Exception ex)
            {
                throw new Exception("Ocurrio un error al intentar conectarse al servidor:" + ex.Message);
            }
            
        }

        private static Entities.Enum.eTablones GetTablonEnum(int value)
        {
            switch (value)
            {
                case 0:
                    return Entities.Enum.eTablones.MilAnuncioRenov;
                case 1:
                    return Entities.Enum.eTablones.MilAnunciosRenovPago;
                default:
                    return Entities.Enum.eTablones.MilAnuncioRenov;
            }
        }
    }
}
