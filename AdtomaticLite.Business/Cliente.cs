﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AdtomaticLite.Business
{
    public class Cliente
    {
        public static bool IsActivo(int idCliente)
        { 
            return AdtomaticLite.WS.Client.Client.IsClienteActivo(idCliente);
        }

        public static string EncryptId(int idCliente)
        {
            string result = string.Empty;
            foreach (char caracter in idCliente.ToString())
            {
                result += (char)((int)caracter + 10);
            }
            return result;
        }

        public static int DecryptId(string encryptedIdCliente)
        {
            string result = string.Empty;
            foreach (char caracter in encryptedIdCliente)
            {
                result += (char)(int)((int)caracter - 10);
            }
            if (string.IsNullOrEmpty(result))
                return -1;
            return int.Parse(result);
        }
    }
}
