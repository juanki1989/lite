﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Win32;

namespace AdtomaticLite.Business
{
    public static class Configuration
    {
        public static int IdCliente
        {
            get 
            {
                string registryValue = (string)Registry.GetValue(@"HKEY_CURRENT_USER\Software\AdtomaticLite", "Id", Cliente.EncryptId(-1));
                if (string.IsNullOrEmpty(registryValue))
                    registryValue = Cliente.EncryptId(-1);
                return Business.Cliente.DecryptId(registryValue); 
            }
            set 
            {
                string encryptedId = Cliente.EncryptId(value);
                Registry.SetValue(@"HKEY_CURRENT_USER\Software\AdtomaticLite", "Id", encryptedId);
                //Properties.Settings.Default.IdCliente = encryptedId;
                //Properties.Settings.Default.Save();
            }
        }
    }
}
