﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace AdtomaticLite.Business
{
    public class Run
    {
        private Thread _thread;

        public void RunAd(Entities.Anuncio anuncio)
        {
            Tablones.TablonBase tablon = GetTablon(anuncio);
            tablon.CargarAnuncio(anuncio);
            _thread = new Thread(() => tablon.Run(false));
            _thread.IsBackground = true;
            _thread.SetApartmentState(ApartmentState.STA);
            _thread.Name = "AdtomaticRenew";
            _thread.Start();
            _thread.Join();
        }

        private Tablones.TablonBase GetTablon(Entities.Anuncio anuncio)
        {
            switch (anuncio.Tipo)
            {
                case Entities.Enum.eTablones.MilAnuncioRenov:
                    return new Tablones.MilAnunciosRenovador();    
                case Entities.Enum.eTablones.MilAnunciosRenovPago:
                    return new Tablones.MilAnunciosRenovadorPago();
                default:
                    return null;
            }
        }
    }
}
