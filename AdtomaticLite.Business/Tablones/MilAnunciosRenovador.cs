﻿using System;
using System.Collections.Generic;
using System.Linq;
using WatiN.Core;

namespace AdtomaticLite.Business.Tablones
{
    public class MilAnunciosRenovador : TablonBase 
    {
        private int _lastPage;
        private const string NAME_MAIL = "email";
        private const string NAME_PASS = "contra";

        public MilAnunciosRenovador()
        {
            _url = "http://www.milanuncios.com/mis-anuncios/";
        }

        protected override void Submit()
        {
            foreach (WatiN.Core.Button wButton in _browser.Forms[0].Buttons)
            {
                if (wButton.Value.Equals("ENVIAR >>"))
                {
                    wButton.Click();
                    break;
                }
            }
            _browser.WaitForComplete();
        }

        protected override void PostSubmit()
        {
            _browser.WaitUntilContainsText("usuario");
            _lastPage = FindLastPage(true);
            _browser.WaitForComplete();
            if (_lastPage == 1)
                _lastPage = FindLastPageByLinks(true);
            _browser.WaitForComplete();
            while (!HasResults() && _lastPage != 1)
            {
                GoToPreviousPage();
            }
            bool wSuccess = RenewLast();
            WaitForComplete();
            FindLogoutImage().Click();
            Close();
            if (wSuccess)
            {
                base.PostSubmit();
            }
            else
            {
                _log.DescResult = "No se renovó ningun anuncio";
                _log.CodigoResultado = -1;
            }     
        }

        protected override void SetValues()
        {
            Image logoutImage = FindLogoutImage();
            if (logoutImage != null) 
            {
                logoutImage.Click();
                WaitForComplete();
            }
            TextField tfName = _browser.TextField(NAME_MAIL);
            tfName.Value = _anuncio.Mail;
            TextField tfPass = _browser.TextField(NAME_PASS);
            tfPass.Value = _anuncio.Password;
            WaitForComplete();
        }

        private int FindLastPageByLinks(bool ClickIt)
        {
            _browser.WaitForComplete();
            int wLastPage = 1;
            TableCell wTableCell = null;
            foreach (WatiN.Core.TableCell wLink in _browser.TableCells)
            {
                string wAttributeValue = wLink.GetAttributeValue("onclick");
                if (wAttributeValue != null && wAttributeValue.Contains("ina="))
                {
                    string wPageNumberStr = wAttributeValue.Substring(wAttributeValue.LastIndexOf("=") + 1);
                    int wPageNumber = Convert.ToInt32(wPageNumberStr.Remove(wPageNumberStr.Length - 2));
                    if (wPageNumber > wLastPage)
                    {
                        wLastPage = wPageNumber;
                        wTableCell = wLink;
                    }
                }
            }
            if (ClickIt && wTableCell != null && wLastPage != 1)
                wTableCell.Click();
            return wLastPage;
        }

        private int FindLastPage(bool GoTo)
        {
            _browser.WaitForComplete();
            int wLastPage = 1;

            InteractivePause(4);
            WaitForComplete();
            
            WatiN.Core.Native.InternetExplorer.IEWaitForComplete wait = new WatiN.Core.Native.InternetExplorer.IEWaitForComplete((WatiN.Core.Native.InternetExplorer.IEBrowser)_browser.NativeBrowser);
            wait.DoWait();
            Table lastTable = _browser.Tables.Last();
            foreach (Element e in lastTable.Elements)
            {
                if (e.TagName == "TD")
                {
                    if ((e.GetAttributeValue("onclick") != null) && (e.GetAttributeValue("onclick").Contains("location")))
                    {
                        string val = e.GetAttributeValue("onclick");
                        int pagNumber = int.Parse(val.Substring(val.Length -2 ,1));
                        if (pagNumber > wLastPage)
                            wLastPage = pagNumber;
                    }
                }
            }

            if (GoTo)
                _browser.GoTo(string.Concat(_browser.Url, "?pagina=", wLastPage));
            return wLastPage;
        }

        private Image FindLogoutImage()
        {
            foreach (Image wImage in _browser.Images)
            {
                if (wImage.Src.Contains("icoSalir.gif"))
                    return wImage;
            }
            return null;
        }

        //private bool RenewLast()
        //{
        //    Link wLastLink = null;
        //    foreach (WatiN.Core.Link wLink in _browser.Links)
        //    {
        //        if (wLink.Url.Contains("javascript:rv"))
        //            wLastLink = wLink;
        //    }
        //    if (wLastLink != null && Renewable())
        //    {
        //        ExecuteByJavaScript(wLastLink); //Por la modificacion de la nueva ventana introducida el 19-07
        //        return true;
        //    }
        //    return false;
        //}

        private bool RenewLast()
        {
            Link wLastLink = null;
            foreach (WatiN.Core.Link wLink in _browser.Links)
            {
                if ((wLink != null) && !(string.IsNullOrEmpty(wLink.Text)) && (wLink.Text.Contains("Renovar")))
                    wLastLink = wLink;
            }
            if (wLastLink != null && Renewable())
            {
                ExecuteByJavaScript(wLastLink); //Por la modificacion de la nueva ventana introducida el 19-07
                return true;
            }
            return false;
        }

        private Div FindLastX6() // X6 = tiempo desde la ult. publicacion
        {
            Div wLastx6 = null;
            foreach (WatiN.Core.Div wDiv in _browser.Divs)
            {
                if (!string.IsNullOrEmpty(wDiv.ClassName) && wDiv.ClassName.Equals("x6"))
                    wLastx6 = wDiv;
            }
            return wLastx6;
        }

        protected virtual bool Renewable()
        {
            string wLastTime = FindLastX6().InnerHtml;
            if (wLastTime.Contains("día"))
                return true;
            else if (wLastTime.Contains("hora") && Convert.ToInt32(wLastTime.Substring(0, 2)) >= 20)
                return true;
            else
                return false;
        }

        private void Close()
        {
            //if (mErrorCode == 0)
            //{
            //    mErrorDescription = "El anuncio se renovo correctamente";
            //    mHtml = mBrowser.DomContainer.Body.OuterText;
            //    mLink = mBrowser.Url.ToString();
            //}
        }

        private bool HasResults()
        {
            if (_browser.ContainsText("No tiene anuncios publicados"))
                return false;
            else
                return true;
        }

        private void GoToPreviousPage()
        {
            _lastPage--;
            _browser.GoTo(string.Concat(_browser.Url.Remove(_browser.Url.IndexOf("?")), "?pagina=", _lastPage));
            _browser.WaitForComplete();
        }

        protected virtual void ExecuteByJavaScript(Link LastLink)
        {
            //string wHref = LastLink.GetAttributeValue("href");
            //string wCode = wHref.Substring(wHref.LastIndexOf(":rv") + 5);
            //wCode = wCode.Remove(wCode.Length - 2);
            LastLink.Click();
            WaitForComplete();
            //LastLink.SetAttributeValue("href", string.Format("javascript:qt('{0}','renovar');", wCode));
            //string jscode = string.Format("javascript:qt('{0}','renovar');", wCode);
            //LastLink.Click();
            _browser.Frames[0].Links[0].Click();
        }
    }
}
