﻿using System;
using System.Collections.Generic;
using System.Linq;
using WatiN.Core;
using System.Diagnostics;
using System.Threading;
using System.Windows.Forms;

namespace AdtomaticLite.Business.Tablones
{
    public class TablonBase
    {
        protected Browser _browser;
        protected string _url;
        protected Entities.Anuncio _anuncio;
        protected Entities.Log _log = new Entities.Log();
        protected int _processId = -1;

        public void CargarAnuncio(Entities.Anuncio anuncio)
        {
            _anuncio = anuncio;
            _anuncio.LastRun = DateTime.Now;
            _log.FechaProgramada = _anuncio.NextRun.Value;
            _log.FechaInicio = DateTime.Now;
        }

        [STAThread]
        public void Run(bool visible)
        {
            try
            {
                Settings.MakeNewIeInstanceVisible = visible;
                Settings.AutoMoveMousePointerToTopLeft = false;
                Settings.AutoCloseDialogs = true;

                using (Browser wBrowser = GetBrowser(false))
                {
                    _browser = wBrowser;
                    _processId = wBrowser.ProcessID;
                    if (_browser != null)

                    LoadPage(_url);

                    SetValues();

                    Submit();

                    PostSubmit();
                }

            }
            catch (Exception ex)
            {
                HandleError(ex);
            }
            finally
            {
                FinalizeRun();
            }
        }

        protected virtual Browser GetBrowser(bool createInNewProcess)
        {
            return new IE(createInNewProcess);
        }

        protected virtual void LoadPage(string url)
        {
            _browser.GoTo(url);
            WaitForComplete();
        }

        protected virtual void SetValues()
        {
        
        }

        protected virtual void Submit()
        {
            _browser.Forms[0].Submit();
            WaitForComplete();
        }

        protected virtual void PostSubmit()
        {
            _log.CodigoResultado = 0;
            _log.DescResult = "Ejecucción exitosa";
        }

        protected virtual void FinalizeRun()
        {
            _log.FechaFin = DateTime.Now;
            _anuncio.Logs.Add(_log);
           /* try
            {
                if (_browser != null )
                {
                    IE wIE = (IE)_browser;
                    wIE.ClearCache();
                    wIE.ClearCookies();
                    //KillIEProcess();
                    //wIE.ProcessID;
                    Process.GetProcessById(_browser.ProcessID).Kill();
                    //KillIEProcess();
                }
            }
            catch (Exception exss)
            {
                MessageBox.Show(exss.Message);
            }*/
            try
            {
            KillIEProcess();    
            }
            catch (Exception exs)
            {
            //MessageBox.Show(exs.Message);
            }
          
            
        }

        protected virtual void KillIEProcess()
        {
            if(_browser!=null)
            Process.GetProcessById(_browser.ProcessID).Dispose();
            Process.GetProcessById(_browser.ProcessID).Close();
            //Process.GetProcessById(_browser.ProcessID).Kill();
            /*var processes = Process.GetProcessesByName("iexplore");
            foreach (var process in processes)
            {
                ((Process)process).Kill();                
             }*/
            /*foreach (System.Diagnostics.Process myProc in System.Diagnostics.Process.GetProcesses())
            {
                if (myProc.ProcessName == "iexplore")
                {
                    myProc.Kill();
                }
            }*/
        }

        protected virtual void WaitForComplete()
        {
            WaitForComplete(3);
        }

        protected virtual void WaitForComplete(int maxLoops)
        {
            // Wait for the page to complete
            _browser.WaitForComplete();
            int i = 0;
            while (_browser.Body == null && i++ <= maxLoops)
            {
                _browser.WaitForComplete(); // (Or other wait/sleep)
            }
        }

        protected void InteractivePause(int seconds)
        {
            TimeSpan length = new TimeSpan(0, 0, seconds);
            DateTime start = DateTime.Now;
            TimeSpan restTime = new TimeSpan(200000); // 20 milliseconds
            while (true)
            {
                System.Windows.Forms.Application.DoEvents();
                TimeSpan remainingTime = start.Add(length).Subtract(DateTime.Now);
                if (remainingTime > restTime)
                {
                    //System.Diagnostics.Debug.WriteLine(string.Format("1: {0}", remainingTime));
                    // Wait an insignificant amount of time so that the
                    // CPU usage doesn't hit the roof while we wait.
                    System.Threading.Thread.Sleep(restTime);
                }
                else
                {
                    //System.Diagnostics.Debug.WriteLine(string.Format("2: {0}", remainingTime));
                    if (remainingTime.Ticks > 0)
                        System.Threading.Thread.Sleep(remainingTime);
                    break;
                }
            }

        }

        protected virtual void HandleError(Exception ex)
        {
            _log.CodigoResultado = -1;
            _log.DescResult = String.Format("Ocurrió un error. {0}", ex.Message);
            
        }

        protected virtual void TiempoSuperadoError(Exception ex)
        {
            String mErrorDescription = "Se ha superado el tiempo maximo de ejecución permitido. Ejecución abortada.";
            //mHtml = mErrorDescription;
            _log.CodigoResultado = -1;
            _log.DescResult = mErrorDescription;
        }
        //private void SendMailOnError(Exception ex)
        //{
        //    if (_anuncio.Tipo != Entities.Enum.eTablones.MilAnunciosRenovPago)
        //        return;
        //    string wFromMail = "adtomaticlite@gmail.com";
        //    string wDisplayName = "Adtomatic Lite";
        //    string wReceiverMail = "adtomaticlite@gmail.com"; //_anuncio.Mail; //TODO
        //    string wSubject = string.Format("Fallo anuncio: {0}", _anuncio.Name);
        //    string wBody = ex.ToString();
        //    string wServer = "smtp.gmail.com";
        //    int wPort = 587;
        //    bool wUseSSL = true;
        //    string wPass = "AdtomaticINSLite";
        //    try
        //    {
        //        ElipsysIT.Common.Mail.SendMail(wFromMail, wDisplayName, wReceiverMail, wSubject, wBody, wServer, wPort, wUseSSL, wPass);
        //    }
        //    catch (Exception)
        //    {
        //    }
        //}


    }
}
