﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace AdtomaticLite.Business.Tablones
{
    public class MilAnunciosRenovadorPago : MilAnunciosRenovador  
    {
        private const string NAME_INTERVALO = "cada";
        private const string NAME_RENUEVADENOCHE = "nocturno";

        public MilAnunciosRenovadorPago()
        {
            _url = "http://www.milanuncios.com/mis-anuncios/";
        }

        protected override void ExecuteByJavaScript(WatiN.Core.Link LastLink)
        {
            LastLink.Click();
            WaitForComplete();
            _browser.Frames[0].SelectList(NAME_INTERVALO).SelectByValue(_anuncio.IntervaloRenovPago.Value.ToString());
            _browser.Frames[0].SelectList(NAME_RENUEVADENOCHE).SelectByValue(_anuncio.RenovarNocturno.Value.ToString());
        }
       
        protected override bool Renewable()
        {
            return true;
        }

        protected override void HandleError(Exception ex)
        {
            base.HandleError(ex);
            //AdtomaticLite.WS.Client.Client.EnviarMail(Business.Configuration.IdCliente, this._anuncio.Name);
        }

    }
}
