﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AdtomaticLite.Business
{
    public class Anuncios
    {
        public static List<Entities.Anuncio> GetAnunciosActivos(int idCliente)
        {
            return AdtomaticLite.WS.Client.Client.GetAnunciosActivos(idCliente);
        }

        private static List<Entities.Anuncio> GetListaDePrueba()
        {
            List<Entities.Anuncio> anuncios = new List<Entities.Anuncio>();
            //Entities.Anuncio anuncio = new Entities.Anuncio();
            //anuncio.Name = "Anuncio de prueba1";
            //anuncio.Mail = "pasiongitana2013@hotmail.com";
            //anuncio.Password = "qwa7";
            //anuncio.Tipo = Entities.Enum.eTablones.MilAnuncioRenov;

            //anuncio.IdRunType = 1;
            //anuncio.StartDate = new DateTime(2013, 10, 14);
            //anuncio.StartTime = new DateTime(2013, 10, 14, 00, 06, 46);
            //anuncio.EndDate = null;
            //anuncio.EndTime = new DateTime(2015, 10, 15, 23, 59, 59);
            //anuncio.TimeInterval = 1;
            //anuncio.RunEveryDay = true;
            //anuncio.RunWeekDays = true;
            //anuncio.RunEveryXDays = null;
            //anuncio.NextRun = anuncio.CalculateNextRunDate();

            //Entities.Anuncio anuncio2 = new Entities.Anuncio();
            //anuncio2.Name = "Anuncio de prueba2";
            //anuncio2.Mail = "pasiongitana2013@hotmail.com";
            //anuncio2.Password = "qwa7";
            //anuncio2.Tipo = Entities.Enum.eTablones.MilAnuncioRenov;

            //anuncio2.IdRunType = 1;
            //anuncio2.StartDate = new DateTime(2013, 10, 14);
            //anuncio2.StartTime = new DateTime(2013, 10, 14, 00, 06, 46);
            //anuncio2.EndDate = null;
            //anuncio2.EndTime = new DateTime(2015, 10, 15, 23, 59, 59);
            //anuncio2.TimeInterval = 1;
            //anuncio2.RunEveryDay = true;
            //anuncio2.RunWeekDays = true;
            //anuncio2.RunEveryXDays = null;
            //anuncio2.NextRun = anuncio2.CalculateNextRunDate();

            //Entities.Anuncio anuncio3 = new Entities.Anuncio();
            //anuncio3.Name = "Anuncio PAGO de prueba3";
            //anuncio3.Mail = "sollcaxrukhe@hotmail.com";
            //anuncio3.Password = "k9pg";
            //anuncio3.Tipo = Entities.Enum.eTablones.MilAnunciosRenovPago;

            //anuncio3.IdRunType = 1;
            //anuncio3.StartDate = new DateTime(2013, 10, 14);
            //anuncio3.StartTime = new DateTime(2013, 10, 14, 00, 06, 46);
            //anuncio3.EndDate = null;
            //anuncio3.EndTime = new DateTime(2015, 10, 15, 23, 59, 59);
            //anuncio3.TimeInterval = 1;
            //anuncio3.RunEveryDay = true;
            //anuncio3.RunWeekDays = true;
            //anuncio3.RunEveryXDays = null;
            //anuncio3.IntervaloRenovPago = 3600;
            //anuncio3.RenovarNocturno = 0;
            //anuncio3.NextRun = anuncio3.CalculateNextRunDate();

            //anuncios.Add(anuncio);
            //anuncios.Add(anuncio2);
            //anuncios.Add(anuncio3);
            
            System.Xml.Serialization.XmlSerializer deserializer = new System.Xml.Serialization.XmlSerializer(anuncios.GetType());
            anuncios = (List<Entities.Anuncio>)deserializer.Deserialize(System.IO.File.Open(System.IO.Path.Combine(System.IO.Directory.GetCurrentDirectory(), "anuncios.xml"), System.IO.FileMode.Open));
            return anuncios;
        }

    }
}
