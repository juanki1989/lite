﻿namespace AdtomaticLite
{
    partial class FrMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrMain));
            this.lblEstado = new System.Windows.Forms.Label();
            this.lblEstadoDesc = new System.Windows.Forms.Label();
            this.pnlInf = new System.Windows.Forms.Panel();
            this.pnlCenter = new System.Windows.Forms.Panel();
            this.checkBoxSeleccionarTodos = new System.Windows.Forms.CheckBox();
            this.checkBoxSeleccionarTodosAutorenueva = new System.Windows.Forms.CheckBox();
            this.panel7 = new System.Windows.Forms.Panel();
            this.olvAnunciosAutorenueva = new BrightIdeasSoftware.ObjectListView();
            this.olvColumn7 = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.olvColumn8 = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.olvColumn9 = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.olvColumn10 = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.olvColumn11 = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.olvColumn12 = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.olvColumn13 = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.panel6 = new System.Windows.Forms.Panel();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.tsbStopAutorenueva = new System.Windows.Forms.ToolStripButton();
            this.tsbPlayAutorenueva = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.tsbLogsAutorenueva = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton3 = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton4 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton5 = new System.Windows.Forms.ToolStripButton();
            this.tsbRefrescarAutorenueva = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator6 = new System.Windows.Forms.ToolStripSeparator();
            this.tsbRecargarSaldo = new System.Windows.Forms.ToolStripButton();
            this.tsbModificarAnuncio = new System.Windows.Forms.ToolStripButton();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.panel5 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.tsMain = new System.Windows.Forms.ToolStrip();
            this.tsbStop = new System.Windows.Forms.ToolStripButton();
            this.tsbPlay = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.tsbRefresh = new System.Windows.Forms.ToolStripButton();
            this.tsbActivate = new System.Windows.Forms.ToolStripButton();
            this.tsbDeActivate = new System.Windows.Forms.ToolStripButton();
            this.tsbLogsGratuitos = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.recargarSaldo = new System.Windows.Forms.ToolStripButton();
            this.editarPublicacion = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.panel1 = new System.Windows.Forms.Panel();
            this.olvAnuncios = new BrightIdeasSoftware.ObjectListView();
            this.olvColumn6 = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.olvColumn1 = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.olvColumn2 = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.olvColumn3 = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.olvColumn4 = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.olvColumn5 = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.pnlSup = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.trayIcon = new System.Windows.Forms.NotifyIcon(this.components);
            this.contextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.restoreToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.timerCheckCliente = new System.Windows.Forms.Timer(this.components);
            this.tbIntervaloEntreAnuncios = new System.Windows.Forms.TextBox();
            this.NameIntervaloEntreAnuncios = new System.Windows.Forms.Label();
            this.pnlInf.SuspendLayout();
            this.pnlCenter.SuspendLayout();
            this.panel7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.olvAnunciosAutorenueva)).BeginInit();
            this.panel6.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel2.SuspendLayout();
            this.tsMain.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.olvAnuncios)).BeginInit();
            this.pnlSup.SuspendLayout();
            this.contextMenuStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblEstado
            // 
            this.lblEstado.AutoSize = true;
            this.lblEstado.Location = new System.Drawing.Point(9, 9);
            this.lblEstado.Name = "lblEstado";
            this.lblEstado.Size = new System.Drawing.Size(43, 13);
            this.lblEstado.TabIndex = 0;
            this.lblEstado.Text = "Estado:";
            this.lblEstado.Visible = false;
            // 
            // lblEstadoDesc
            // 
            this.lblEstadoDesc.AutoSize = true;
            this.lblEstadoDesc.ForeColor = System.Drawing.Color.Red;
            this.lblEstadoDesc.Location = new System.Drawing.Point(58, 9);
            this.lblEstadoDesc.Name = "lblEstadoDesc";
            this.lblEstadoDesc.Size = new System.Drawing.Size(62, 13);
            this.lblEstadoDesc.TabIndex = 1;
            this.lblEstadoDesc.Text = "Iniciando ...";
            this.lblEstadoDesc.Visible = false;
            // 
            // pnlInf
            // 
            this.pnlInf.Controls.Add(this.lblEstado);
            this.pnlInf.Controls.Add(this.lblEstadoDesc);
            this.pnlInf.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnlInf.Location = new System.Drawing.Point(0, 655);
            this.pnlInf.Name = "pnlInf";
            this.pnlInf.Size = new System.Drawing.Size(1028, 33);
            this.pnlInf.TabIndex = 2;
            // 
            // pnlCenter
            // 
            this.pnlCenter.Controls.Add(this.NameIntervaloEntreAnuncios);
            this.pnlCenter.Controls.Add(this.tbIntervaloEntreAnuncios);
            this.pnlCenter.Controls.Add(this.checkBoxSeleccionarTodos);
            this.pnlCenter.Controls.Add(this.checkBoxSeleccionarTodosAutorenueva);
            this.pnlCenter.Controls.Add(this.panel7);
            this.pnlCenter.Controls.Add(this.panel6);
            this.pnlCenter.Controls.Add(this.panel3);
            this.pnlCenter.Controls.Add(this.panel2);
            this.pnlCenter.Controls.Add(this.panel1);
            this.pnlCenter.Controls.Add(this.pnlSup);
            this.pnlCenter.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlCenter.Location = new System.Drawing.Point(0, 0);
            this.pnlCenter.Name = "pnlCenter";
            this.pnlCenter.Size = new System.Drawing.Size(1028, 655);
            this.pnlCenter.TabIndex = 3;
            // 
            // checkBoxSeleccionarTodos
            // 
            this.checkBoxSeleccionarTodos.AutoSize = true;
            this.checkBoxSeleccionarTodos.Location = new System.Drawing.Point(5, 75);
            this.checkBoxSeleccionarTodos.Name = "checkBoxSeleccionarTodos";
            this.checkBoxSeleccionarTodos.Size = new System.Drawing.Size(115, 17);
            this.checkBoxSeleccionarTodos.TabIndex = 1;
            this.checkBoxSeleccionarTodos.Text = "Seleccionar Todos";
            this.checkBoxSeleccionarTodos.UseVisualStyleBackColor = true;
            this.checkBoxSeleccionarTodos.CheckedChanged += new System.EventHandler(this.checkBoxSeleccionarTodos_CheckedChanged);
            // 
            // checkBoxSeleccionarTodosAutorenueva
            // 
            this.checkBoxSeleccionarTodosAutorenueva.AutoSize = true;
            this.checkBoxSeleccionarTodosAutorenueva.Location = new System.Drawing.Point(5, 398);
            this.checkBoxSeleccionarTodosAutorenueva.Name = "checkBoxSeleccionarTodosAutorenueva";
            this.checkBoxSeleccionarTodosAutorenueva.Size = new System.Drawing.Size(115, 17);
            this.checkBoxSeleccionarTodosAutorenueva.TabIndex = 1;
            this.checkBoxSeleccionarTodosAutorenueva.Text = "Seleccionar Todos";
            this.checkBoxSeleccionarTodosAutorenueva.UseVisualStyleBackColor = true;
            this.checkBoxSeleccionarTodosAutorenueva.CheckedChanged += new System.EventHandler(this.checkBoxSeleccionarTodosAutorenueva_CheckedChanged);
            // 
            // panel7
            // 
            this.panel7.Controls.Add(this.olvAnunciosAutorenueva);
            this.panel7.Location = new System.Drawing.Point(3, 415);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(1001, 237);
            this.panel7.TabIndex = 6;
            // 
            // olvAnunciosAutorenueva
            // 
            this.olvAnunciosAutorenueva.AllColumns.Add(this.olvColumn7);
            this.olvAnunciosAutorenueva.AllColumns.Add(this.olvColumn8);
            this.olvAnunciosAutorenueva.AllColumns.Add(this.olvColumn9);
            this.olvAnunciosAutorenueva.AllColumns.Add(this.olvColumn10);
            this.olvAnunciosAutorenueva.AllColumns.Add(this.olvColumn11);
            this.olvAnunciosAutorenueva.AllColumns.Add(this.olvColumn12);
            this.olvAnunciosAutorenueva.AllColumns.Add(this.olvColumn13);
            this.olvAnunciosAutorenueva.AlternateRowBackColor = System.Drawing.Color.AliceBlue;
            this.olvAnunciosAutorenueva.CheckBoxes = true;
            this.olvAnunciosAutorenueva.CheckedAspectName = "check";
            this.olvAnunciosAutorenueva.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.olvColumn7,
            this.olvColumn8,
            this.olvColumn9,
            this.olvColumn10,
            this.olvColumn11,
            this.olvColumn12,
            this.olvColumn13});
            this.olvAnunciosAutorenueva.Dock = System.Windows.Forms.DockStyle.Fill;
            this.olvAnunciosAutorenueva.FullRowSelect = true;
            this.olvAnunciosAutorenueva.GridLines = true;
            this.olvAnunciosAutorenueva.HideSelection = false;
            this.olvAnunciosAutorenueva.Location = new System.Drawing.Point(0, 0);
            this.olvAnunciosAutorenueva.MultiSelect = false;
            this.olvAnunciosAutorenueva.Name = "olvAnunciosAutorenueva";
            this.olvAnunciosAutorenueva.ShowGroups = false;
            this.olvAnunciosAutorenueva.Size = new System.Drawing.Size(1001, 237);
            this.olvAnunciosAutorenueva.SortGroupItemsByPrimaryColumn = false;
            this.olvAnunciosAutorenueva.Sorting = System.Windows.Forms.SortOrder.Ascending;
            this.olvAnunciosAutorenueva.TabIndex = 6;
            this.olvAnunciosAutorenueva.UseAlternatingBackColors = true;
            this.olvAnunciosAutorenueva.UseCompatibleStateImageBehavior = false;
            this.olvAnunciosAutorenueva.View = System.Windows.Forms.View.Details;
            // 
            // olvColumn7
            // 
            this.olvColumn7.AspectName = "";
            this.olvColumn7.CheckBoxes = global::AdtomaticLite.Properties.Settings.Default.check;
            this.olvColumn7.Text = "";
            this.olvColumn7.TriStateCheckBoxes = true;
            this.olvColumn7.Width = 27;
            // 
            // olvColumn8
            // 
            this.olvColumn8.AspectName = "Name";
            this.olvColumn8.DisplayIndex = 2;
            this.olvColumn8.Text = "Nombre";
            this.olvColumn8.Width = 304;
            // 
            // olvColumn9
            // 
            this.olvColumn9.AspectName = "startDefault";
            this.olvColumn9.DisplayIndex = 3;
            this.olvColumn9.Text = "activacion por defecto";
            this.olvColumn9.Width = 178;
            // 
            // olvColumn10
            // 
            this.olvColumn10.AspectName = "endDefault";
            this.olvColumn10.DisplayIndex = 4;
            this.olvColumn10.Text = "desactivacion por defecto";
            this.olvColumn10.Width = 155;
            // 
            // olvColumn11
            // 
            this.olvColumn11.AspectName = "DescLastRun";
            this.olvColumn11.DisplayIndex = 5;
            this.olvColumn11.Text = "Resultado última ejecucción";
            this.olvColumn11.Width = 205;
            // 
            // olvColumn12
            // 
            this.olvColumn12.AspectName = "Saldo";
            this.olvColumn12.DisplayIndex = 6;
            this.olvColumn12.IsEditable = false;
            this.olvColumn12.Text = "saldo";
            // 
            // olvColumn13
            // 
            this.olvColumn13.AspectName = "activo";
            this.olvColumn13.DisplayIndex = 1;
            this.olvColumn13.Text = "Estado";
            this.olvColumn13.Width = 70;
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.toolStrip1);
            this.panel6.Location = new System.Drawing.Point(7, 351);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(988, 44);
            this.panel6.TabIndex = 5;
            // 
            // toolStrip1
            // 
            this.toolStrip1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.toolStrip1.ImageScalingSize = new System.Drawing.Size(32, 32);
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsbStopAutorenueva,
            this.tsbPlayAutorenueva,
            this.toolStripSeparator4,
            this.tsbLogsAutorenueva,
            this.toolStripButton3,
            this.toolStripSeparator5,
            this.toolStripButton4,
            this.toolStripButton5,
            this.tsbRefrescarAutorenueva,
            this.toolStripSeparator6,
            this.tsbRecargarSaldo,
            this.tsbModificarAnuncio});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(988, 44);
            this.toolStrip1.TabIndex = 5;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // tsbStopAutorenueva
            // 
            this.tsbStopAutorenueva.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbStopAutorenueva.Image = ((System.Drawing.Image)(resources.GetObject("tsbStopAutorenueva.Image")));
            this.tsbStopAutorenueva.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbStopAutorenueva.Name = "tsbStopAutorenueva";
            this.tsbStopAutorenueva.Size = new System.Drawing.Size(36, 41);
            this.tsbStopAutorenueva.Text = "Detener";
            this.tsbStopAutorenueva.Click += new System.EventHandler(this.stbStopAutorenueva_Click);
            // 
            // tsbPlayAutorenueva
            // 
            this.tsbPlayAutorenueva.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbPlayAutorenueva.Image = ((System.Drawing.Image)(resources.GetObject("tsbPlayAutorenueva.Image")));
            this.tsbPlayAutorenueva.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbPlayAutorenueva.Name = "tsbPlayAutorenueva";
            this.tsbPlayAutorenueva.Size = new System.Drawing.Size(36, 41);
            this.tsbPlayAutorenueva.Text = "Iniciar";
            this.tsbPlayAutorenueva.Click += new System.EventHandler(this.stbPlayAutorenueva_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(6, 44);
            // 
            // tsbLogsAutorenueva
            // 
            this.tsbLogsAutorenueva.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbLogsAutorenueva.Image = ((System.Drawing.Image)(resources.GetObject("tsbLogsAutorenueva.Image")));
            this.tsbLogsAutorenueva.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbLogsAutorenueva.Name = "tsbLogsAutorenueva";
            this.tsbLogsAutorenueva.Size = new System.Drawing.Size(36, 41);
            this.tsbLogsAutorenueva.Text = "Logs";
            this.tsbLogsAutorenueva.Click += new System.EventHandler(this.tsbLogsAutorenueva_Click);
            // 
            // toolStripButton3
            // 
            this.toolStripButton3.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton3.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton3.Image")));
            this.toolStripButton3.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton3.Name = "toolStripButton3";
            this.toolStripButton3.Size = new System.Drawing.Size(36, 41);
            this.toolStripButton3.Text = "Sincronizar con central";
            this.toolStripButton3.Visible = false;
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(6, 44);
            this.toolStripSeparator5.Visible = false;
            // 
            // toolStripButton4
            // 
            this.toolStripButton4.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton4.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton4.Image")));
            this.toolStripButton4.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton4.Name = "toolStripButton4";
            this.toolStripButton4.Size = new System.Drawing.Size(36, 41);
            this.toolStripButton4.Text = "Activar anuncio pago";
            this.toolStripButton4.Visible = false;
            // 
            // toolStripButton5
            // 
            this.toolStripButton5.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton5.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton5.Image")));
            this.toolStripButton5.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton5.Name = "toolStripButton5";
            this.toolStripButton5.Size = new System.Drawing.Size(36, 41);
            this.toolStripButton5.Text = "Desactivar anuncio pago";
            this.toolStripButton5.Visible = false;
            // 
            // tsbRefrescarAutorenueva
            // 
            this.tsbRefrescarAutorenueva.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.tsbRefrescarAutorenueva.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbRefrescarAutorenueva.Image = ((System.Drawing.Image)(resources.GetObject("tsbRefrescarAutorenueva.Image")));
            this.tsbRefrescarAutorenueva.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbRefrescarAutorenueva.Name = "tsbRefrescarAutorenueva";
            this.tsbRefrescarAutorenueva.Size = new System.Drawing.Size(36, 41);
            this.tsbRefrescarAutorenueva.Text = "Refrescar Saldos";
            this.tsbRefrescarAutorenueva.Click += new System.EventHandler(this.toolStripButton7_Click);
            // 
            // toolStripSeparator6
            // 
            this.toolStripSeparator6.Name = "toolStripSeparator6";
            this.toolStripSeparator6.Size = new System.Drawing.Size(6, 44);
            // 
            // tsbRecargarSaldo
            // 
            this.tsbRecargarSaldo.AutoSize = false;
            this.tsbRecargarSaldo.Image = ((System.Drawing.Image)(resources.GetObject("tsbRecargarSaldo.Image")));
            this.tsbRecargarSaldo.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsbRecargarSaldo.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbRecargarSaldo.Name = "tsbRecargarSaldo";
            this.tsbRecargarSaldo.Size = new System.Drawing.Size(125, 41);
            this.tsbRecargarSaldo.ToolTipText = "Recargar Saldo";
            this.tsbRecargarSaldo.Click += new System.EventHandler(this.tsbRecargarSaldo_Click);
            // 
            // tsbModificarAnuncio
            // 
            this.tsbModificarAnuncio.AutoSize = false;
            this.tsbModificarAnuncio.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbModificarAnuncio.Image = ((System.Drawing.Image)(resources.GetObject("tsbModificarAnuncio.Image")));
            this.tsbModificarAnuncio.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsbModificarAnuncio.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbModificarAnuncio.Name = "tsbModificarAnuncio";
            this.tsbModificarAnuncio.Size = new System.Drawing.Size(125, 41);
            this.tsbModificarAnuncio.ToolTipText = "Editar Publicacion";
            this.tsbModificarAnuncio.Click += new System.EventHandler(this.tsbModificarAnuncio_Click);
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.panel4);
            this.panel3.Location = new System.Drawing.Point(7, 315);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(997, 31);
            this.panel3.TabIndex = 4;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.panel5);
            this.panel4.Controls.Add(this.label2);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel4.Location = new System.Drawing.Point(0, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(997, 33);
            this.panel4.TabIndex = 2;
            // 
            // panel5
            // 
            this.panel5.Location = new System.Drawing.Point(0, 32);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(994, 48);
            this.panel5.TabIndex = 5;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(3, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(100, 20);
            this.label2.TabIndex = 0;
            this.label2.Text = "Autorenueva";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.tsMain);
            this.panel2.Location = new System.Drawing.Point(7, 32);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(997, 40);
            this.panel2.TabIndex = 3;
            // 
            // tsMain
            // 
            this.tsMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tsMain.ImageScalingSize = new System.Drawing.Size(32, 32);
            this.tsMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsbStop,
            this.tsbPlay,
            this.toolStripSeparator1,
            this.tsbRefresh,
            this.tsbActivate,
            this.tsbDeActivate,
            this.tsbLogsGratuitos,
            this.toolStripSeparator3,
            this.recargarSaldo,
            this.editarPublicacion,
            this.toolStripSeparator2});
            this.tsMain.Location = new System.Drawing.Point(0, 0);
            this.tsMain.Name = "tsMain";
            this.tsMain.Size = new System.Drawing.Size(997, 40);
            this.tsMain.TabIndex = 4;
            this.tsMain.Text = "toolStrip1";
            // 
            // tsbStop
            // 
            this.tsbStop.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbStop.Image = ((System.Drawing.Image)(resources.GetObject("tsbStop.Image")));
            this.tsbStop.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbStop.Name = "tsbStop";
            this.tsbStop.Size = new System.Drawing.Size(36, 37);
            this.tsbStop.Text = "Detener";
            this.tsbStop.Click += new System.EventHandler(this.tsbStop_Click_1);
            // 
            // tsbPlay
            // 
            this.tsbPlay.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbPlay.Image = ((System.Drawing.Image)(resources.GetObject("tsbPlay.Image")));
            this.tsbPlay.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbPlay.Name = "tsbPlay";
            this.tsbPlay.Size = new System.Drawing.Size(36, 37);
            this.tsbPlay.Text = "Iniciar";
            this.tsbPlay.Click += new System.EventHandler(this.tsbPlay_Click_1);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 40);
            // 
            // tsbRefresh
            // 
            this.tsbRefresh.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbRefresh.Image = ((System.Drawing.Image)(resources.GetObject("tsbRefresh.Image")));
            this.tsbRefresh.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbRefresh.Name = "tsbRefresh";
            this.tsbRefresh.Size = new System.Drawing.Size(36, 37);
            this.tsbRefresh.Text = "Sincronizar con central";
            this.tsbRefresh.Visible = false;
            // 
            // tsbActivate
            // 
            this.tsbActivate.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbActivate.Image = ((System.Drawing.Image)(resources.GetObject("tsbActivate.Image")));
            this.tsbActivate.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbActivate.Name = "tsbActivate";
            this.tsbActivate.Size = new System.Drawing.Size(36, 37);
            this.tsbActivate.Text = "Activar anuncio pago";
            this.tsbActivate.Visible = false;
            // 
            // tsbDeActivate
            // 
            this.tsbDeActivate.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbDeActivate.Image = ((System.Drawing.Image)(resources.GetObject("tsbDeActivate.Image")));
            this.tsbDeActivate.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbDeActivate.Name = "tsbDeActivate";
            this.tsbDeActivate.Size = new System.Drawing.Size(36, 37);
            this.tsbDeActivate.Text = "Desactivar anuncio pago";
            this.tsbDeActivate.Visible = false;
            // 
            // tsbLogsGratuitos
            // 
            this.tsbLogsGratuitos.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbLogsGratuitos.Image = ((System.Drawing.Image)(resources.GetObject("tsbLogsGratuitos.Image")));
            this.tsbLogsGratuitos.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbLogsGratuitos.Name = "tsbLogsGratuitos";
            this.tsbLogsGratuitos.Size = new System.Drawing.Size(36, 37);
            this.tsbLogsGratuitos.Text = "Logs";
            this.tsbLogsGratuitos.Click += new System.EventHandler(this.tsbLogs_Click_1);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 40);
            // 
            // recargarSaldo
            // 
            this.recargarSaldo.Image = ((System.Drawing.Image)(resources.GetObject("recargarSaldo.Image")));
            this.recargarSaldo.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.recargarSaldo.Name = "recargarSaldo";
            this.recargarSaldo.Size = new System.Drawing.Size(36, 37);
            this.recargarSaldo.ToolTipText = "Recargar Saldo";
            this.recargarSaldo.Visible = false;
            // 
            // editarPublicacion
            // 
            this.editarPublicacion.Image = ((System.Drawing.Image)(resources.GetObject("editarPublicacion.Image")));
            this.editarPublicacion.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.editarPublicacion.Name = "editarPublicacion";
            this.editarPublicacion.Size = new System.Drawing.Size(36, 37);
            this.editarPublicacion.ToolTipText = "Editar Publicacion";
            this.editarPublicacion.Visible = false;
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 40);
            this.toolStripSeparator2.Visible = false;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.olvAnuncios);
            this.panel1.Location = new System.Drawing.Point(3, 98);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1001, 211);
            this.panel1.TabIndex = 2;
            // 
            // olvAnuncios
            // 
            this.olvAnuncios.AllColumns.Add(this.olvColumn6);
            this.olvAnuncios.AllColumns.Add(this.olvColumn1);
            this.olvAnuncios.AllColumns.Add(this.olvColumn2);
            this.olvAnuncios.AllColumns.Add(this.olvColumn3);
            this.olvAnuncios.AllColumns.Add(this.olvColumn4);
            this.olvAnuncios.AllColumns.Add(this.olvColumn5);
            this.olvAnuncios.AlternateRowBackColor = System.Drawing.Color.AliceBlue;
            this.olvAnuncios.BackColor = System.Drawing.Color.White;
            this.olvAnuncios.CellEditTabChangesRows = true;
            this.olvAnuncios.CheckBoxes = true;
            this.olvAnuncios.CheckedAspectName = "check";
            this.olvAnuncios.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.olvColumn6,
            this.olvColumn1,
            this.olvColumn2,
            this.olvColumn3,
            this.olvColumn4,
            this.olvColumn5});
            this.olvAnuncios.Dock = System.Windows.Forms.DockStyle.Fill;
            this.olvAnuncios.FullRowSelect = true;
            this.olvAnuncios.GridLines = true;
            this.olvAnuncios.Location = new System.Drawing.Point(0, 0);
            this.olvAnuncios.MultiSelect = false;
            this.olvAnuncios.Name = "olvAnuncios";
            this.olvAnuncios.ShowGroups = false;
            this.olvAnuncios.Size = new System.Drawing.Size(1001, 211);
            this.olvAnuncios.SortGroupItemsByPrimaryColumn = false;
            this.olvAnuncios.Sorting = System.Windows.Forms.SortOrder.Ascending;
            this.olvAnuncios.TabIndex = 7;
            this.olvAnuncios.UseAlternatingBackColors = true;
            this.olvAnuncios.UseCompatibleStateImageBehavior = false;
            this.olvAnuncios.View = System.Windows.Forms.View.Details;
            this.olvAnuncios.SelectedIndexChanged += new System.EventHandler(this.olvAnuncios_SelectedIndexChanged_1);
            // 
            // olvColumn6
            // 
            this.olvColumn6.AspectName = "";
            this.olvColumn6.CheckBoxes = global::AdtomaticLite.Properties.Settings.Default.check;
            this.olvColumn6.Text = "";
            this.olvColumn6.TriStateCheckBoxes = true;
            this.olvColumn6.Width = 27;
            // 
            // olvColumn1
            // 
            this.olvColumn1.AspectName = "Name";
            this.olvColumn1.DisplayIndex = 2;
            this.olvColumn1.Text = "Nombre";
            this.olvColumn1.Width = 304;
            // 
            // olvColumn2
            // 
            this.olvColumn2.AspectName = "NextRun2";
            this.olvColumn2.DisplayIndex = 3;
            this.olvColumn2.Text = "Proxima ejecucción";
            this.olvColumn2.Width = 178;
            // 
            // olvColumn3
            // 
            this.olvColumn3.AspectName = "LastRun";
            this.olvColumn3.DisplayIndex = 4;
            this.olvColumn3.Text = "Última ejecucción";
            this.olvColumn3.Width = 155;
            // 
            // olvColumn4
            // 
            this.olvColumn4.AspectName = "DescLastRun";
            this.olvColumn4.DisplayIndex = 5;
            this.olvColumn4.Text = "Resultado última ejecucción";
            this.olvColumn4.Width = 234;
            // 
            // olvColumn5
            // 
            this.olvColumn5.AspectName = "activo";
            this.olvColumn5.DisplayIndex = 1;
            this.olvColumn5.Text = "Estado";
            this.olvColumn5.Width = 70;
            // 
            // pnlSup
            // 
            this.pnlSup.Controls.Add(this.label1);
            this.pnlSup.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlSup.Location = new System.Drawing.Point(0, 0);
            this.pnlSup.Name = "pnlSup";
            this.pnlSup.Size = new System.Drawing.Size(1028, 33);
            this.pnlSup.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(3, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(145, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "Anuncios Gratuitos";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // trayIcon
            // 
            this.trayIcon.BalloonTipIcon = System.Windows.Forms.ToolTipIcon.Info;
            this.trayIcon.BalloonTipText = "Adtomatic Lite continua su ejecucción de forma minimizada";
            this.trayIcon.BalloonTipTitle = "Adtomatic Lite";
            this.trayIcon.ContextMenuStrip = this.contextMenuStrip;
            this.trayIcon.Icon = ((System.Drawing.Icon)(resources.GetObject("trayIcon.Icon")));
            this.trayIcon.Text = "Adtomatic Lite";
            this.trayIcon.DoubleClick += new System.EventHandler(this.trayIcon_DoubleClick);
            // 
            // contextMenuStrip
            // 
            this.contextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.restoreToolStripMenuItem});
            this.contextMenuStrip.Name = "contextMenuStrip";
            this.contextMenuStrip.Size = new System.Drawing.Size(114, 26);
            // 
            // restoreToolStripMenuItem
            // 
            this.restoreToolStripMenuItem.Name = "restoreToolStripMenuItem";
            this.restoreToolStripMenuItem.Size = new System.Drawing.Size(113, 22);
            this.restoreToolStripMenuItem.Text = "Restore";
            this.restoreToolStripMenuItem.Click += new System.EventHandler(this.tsmiRestore_Click);
            // 
            // timerCheckCliente
            // 
            this.timerCheckCliente.Enabled = true;
            this.timerCheckCliente.Interval = 10800000;
            this.timerCheckCliente.Tick += new System.EventHandler(this.timerCheckCliente_Tick);
            // 
            // tbIntervaloEntreAnuncios
            // 
            this.tbIntervaloEntreAnuncios.Location = new System.Drawing.Point(126, 73);
            this.tbIntervaloEntreAnuncios.Name = "tbIntervaloEntreAnuncios";
            this.tbIntervaloEntreAnuncios.Size = new System.Drawing.Size(32, 20);
            this.tbIntervaloEntreAnuncios.TabIndex = 7;
            this.tbIntervaloEntreAnuncios.Text = "2";
            // 
            // NameIntervaloEntreAnuncios
            // 
            this.NameIntervaloEntreAnuncios.AutoSize = true;
            this.NameIntervaloEntreAnuncios.Location = new System.Drawing.Point(164, 76);
            this.NameIntervaloEntreAnuncios.Name = "NameIntervaloEntreAnuncios";
            this.NameIntervaloEntreAnuncios.Size = new System.Drawing.Size(165, 13);
            this.NameIntervaloEntreAnuncios.TabIndex = 8;
            this.NameIntervaloEntreAnuncios.Text = "intervalo entre anuncios (minutos)";
            // 
            // FrMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1028, 688);
            this.Controls.Add(this.pnlCenter);
            this.Controls.Add(this.pnlInf);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.IsMdiContainer = true;
            this.Name = "FrMain";
            this.Text = "Adtomatic Lite";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FrMain_FormClosed);
            this.Load += new System.EventHandler(this.FrMain_Load);
            this.Resize += new System.EventHandler(this.FrMain_Resize);
            this.pnlInf.ResumeLayout(false);
            this.pnlInf.PerformLayout();
            this.pnlCenter.ResumeLayout(false);
            this.pnlCenter.PerformLayout();
            this.panel7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.olvAnunciosAutorenueva)).EndInit();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.tsMain.ResumeLayout(false);
            this.tsMain.PerformLayout();
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.olvAnuncios)).EndInit();
            this.pnlSup.ResumeLayout(false);
            this.pnlSup.PerformLayout();
            this.contextMenuStrip.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lblEstado;
        private System.Windows.Forms.Label lblEstadoDesc;
        private System.Windows.Forms.Panel pnlInf;
        private System.Windows.Forms.Panel pnlCenter;
        private System.Windows.Forms.Panel pnlSup;
        private System.Windows.Forms.NotifyIcon trayIcon;
        private System.Windows.Forms.Timer timerCheckCliente;
        private System.Windows.Forms.CheckBox checkBoxSeleccionarTodos;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton tsbStopAutorenueva;
        private System.Windows.Forms.ToolStripButton tsbPlayAutorenueva;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripButton toolStripButton3;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripButton toolStripButton4;
        private System.Windows.Forms.ToolStripButton toolStripButton5;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator6;
        private System.Windows.Forms.ToolStripButton tsbLogsAutorenueva;
        private System.Windows.Forms.ToolStripButton tsbRefrescarAutorenueva;
        private System.Windows.Forms.ToolStripButton tsbRecargarSaldo;
        private System.Windows.Forms.ToolStripButton tsbModificarAnuncio;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.CheckBox checkBoxSeleccionarTodosAutorenueva;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ToolStrip tsMain;
        private System.Windows.Forms.ToolStripButton tsbStop;
        private System.Windows.Forms.ToolStripButton tsbPlay;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton tsbRefresh;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripButton tsbActivate;
        private System.Windows.Forms.ToolStripButton tsbDeActivate;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripButton tsbLogsGratuitos;
        private System.Windows.Forms.ToolStripButton recargarSaldo;
        private System.Windows.Forms.ToolStripButton editarPublicacion;
        private BrightIdeasSoftware.ObjectListView olvAnunciosAutorenueva;
        private BrightIdeasSoftware.OLVColumn olvColumn7;
        private BrightIdeasSoftware.OLVColumn olvColumn8;
        private BrightIdeasSoftware.OLVColumn olvColumn9;
        private BrightIdeasSoftware.OLVColumn olvColumn10;
        private BrightIdeasSoftware.OLVColumn olvColumn11;
        private BrightIdeasSoftware.OLVColumn olvColumn12;
        private BrightIdeasSoftware.ObjectListView olvAnuncios;
        private BrightIdeasSoftware.OLVColumn olvColumn6;
        private BrightIdeasSoftware.OLVColumn olvColumn1;
        private BrightIdeasSoftware.OLVColumn olvColumn2;
        private BrightIdeasSoftware.OLVColumn olvColumn3;
        private BrightIdeasSoftware.OLVColumn olvColumn4;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem restoreToolStripMenuItem;
        private BrightIdeasSoftware.OLVColumn olvColumn5;
        private BrightIdeasSoftware.OLVColumn olvColumn13;
        private System.Windows.Forms.Label NameIntervaloEntreAnuncios;
        private System.Windows.Forms.TextBox tbIntervaloEntreAnuncios;


    }
}

