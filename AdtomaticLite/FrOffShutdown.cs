﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace AdtomaticLite
{
    public partial class FrOffShutdown : Form
    {
        private List<Entities.Anuncio> _anuncios;

        public FrOffShutdown()
        {
            InitializeComponent();
        }

        public void Initialize(List<Entities.Anuncio> anuncios)
        {
            _anuncios = anuncios;
            lblCount.Text = string.Format("Progreso: 1/{0}", _anuncios.Count);
        }

        public void RunOffAds()
        {
            bwgWorker.RunWorkerAsync();
        }

        private void bwgWorker_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            int i = 0;
            foreach (Entities.Anuncio anuncio in _anuncios)
            {
                i++;
                if (lblCount.InvokeRequired)
                {
                    lblCount.Invoke(new MethodInvoker(delegate
                    {
                        lblCount.Text = string.Format("Progreso: {0}/{1}", i, _anuncios.Count);
                    }));
                }
                if (lblAnuncio.InvokeRequired)
                {
                    lblAnuncio.Invoke(new MethodInvoker(delegate
                    {
                        lblAnuncio.Text = string.Format("Ejecutando anuncio: {0}", anuncio.Name);
                    }));
                }
                Business.Run run = new Business.Run();
                run.RunAd(anuncio);
            }
            if (this.InvokeRequired)
            {
                this.Invoke(new MethodInvoker(delegate
                {
                    this.Close();
                }));
            } 
        }
    }
}
