﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace AdtomaticLite
{
    public partial class FrLogDetail : Form
    {
        public FrLogDetail()
        {
            InitializeComponent();
        }

        public void Initialize(string detail)
        {
            txtDetail.Text = detail;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
