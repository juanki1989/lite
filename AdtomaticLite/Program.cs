﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using System.Threading;
using System.Diagnostics;

namespace AdtomaticLite
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            bool instanceCountOne = false;

            using (Mutex mtex = new Mutex(true, "AdtomaticLiteAppElipsysIT", out instanceCountOne))
            {

                foreach (Process wProcess in Process.GetProcessesByName("AdtomaticLite"))
                {
                    if (wProcess.Id != Process.GetCurrentProcess().Id)
                    {
                        instanceCountOne = false;
                    }
                }
                if (instanceCountOne)
                {
                    Application.EnableVisualStyles();
                    Application.SetCompatibleTextRenderingDefault(false);
                    Application.Run(new FrMain());
                    mtex.ReleaseMutex();
                }
                else
                {
                    MessageBox.Show("Adtomatic Lite ya esta ejecutandose.", "Adtomatic Lite");
                }
            }


        }
    }
}
