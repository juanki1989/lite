﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace AdtomaticLite
{
    public partial class FrLogs : Form
    {
        private List<Entities.Log> _logs;

        public FrLogs()
        {
            InitializeComponent();
        }
        #region Methods

        public void Initialize(List<Entities.Log> logs)
        {
            _logs = logs;
            olvLogs.SetObjects(_logs);
        }

        #endregion

        #region Common

        private void OpenLogDetail()
        {
            if (Selected == null)
                return;
            FrLogDetail frlogdetail = new FrLogDetail();
            frlogdetail.Initialize(Selected.DescResult);
            frlogdetail.ShowDialog();
        }

        #endregion

        #region Events

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void tsbDetalle_Click(object sender, EventArgs e)
        {
            this.OpenLogDetail();
        }

        #endregion

        #region Properties

        public Entities.Log Selected
        {
            get
            {
                if (olvLogs.SelectedObject == null)
                    return null;
                return (Entities.Log)olvLogs.SelectedObject;
            }
        }

        #endregion

    }
}
