﻿namespace AdtomaticLite
{
    partial class FrOffShutdown
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrOffShutdown));
            this.lblAnuncio = new System.Windows.Forms.Label();
            this.lblCount = new System.Windows.Forms.Label();
            this.bwgWorker = new System.ComponentModel.BackgroundWorker();
            this.SuspendLayout();
            // 
            // lblAnuncio
            // 
            this.lblAnuncio.AutoSize = true;
            this.lblAnuncio.Location = new System.Drawing.Point(12, 9);
            this.lblAnuncio.Name = "lblAnuncio";
            this.lblAnuncio.Size = new System.Drawing.Size(105, 13);
            this.lblAnuncio.TabIndex = 0;
            this.lblAnuncio.Text = "Ejecutando anuncio:";
            // 
            // lblCount
            // 
            this.lblCount.AutoSize = true;
            this.lblCount.Location = new System.Drawing.Point(12, 41);
            this.lblCount.Name = "lblCount";
            this.lblCount.Size = new System.Drawing.Size(68, 13);
            this.lblCount.TabIndex = 1;
            this.lblCount.Text = "Progreso: */*";
            // 
            // bwgWorker
            // 
            this.bwgWorker.WorkerReportsProgress = true;
            this.bwgWorker.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bwgWorker_DoWork);
            // 
            // FrOffShutdown
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(348, 71);
            this.ControlBox = false;
            this.Controls.Add(this.lblCount);
            this.Controls.Add(this.lblAnuncio);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FrOffShutdown";
            this.ShowInTaskbar = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.Text = "Cerrando Adtomatic Lite";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblAnuncio;
        private System.Windows.Forms.Label lblCount;
        private System.ComponentModel.BackgroundWorker bwgWorker;
    }
}