﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace AdtomaticLite
{
    public partial class FrConfigCliente : Form
    {
        private bool _isConfigured = false;

        public FrConfigCliente()
        {
            InitializeComponent();
        }

        private bool Check()
        {
            _ep.SetError(txtIdCliente, string.Empty);
            if (string.IsNullOrEmpty(txtIdCliente.Text))
            {
                _ep.SetError(txtIdCliente, "Debe completar el campo");
                return false;
            }
            int a;
            if (!int.TryParse(txtIdCliente.Text, out a))
            {
                _ep.SetError(txtIdCliente, "El valor debe ser numerico");
                return false;
            }
            return true;
        }
        #region Events

        private void btnOK_Click(object sender, EventArgs e)
        {
            if (Check())
            {
                Business.Configuration.IdCliente = int.Parse(txtIdCliente.Text);
                _isConfigured = true;
                this.Close();
            }
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void FrConfigCliente_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (!_isConfigured)
                Application.Exit();
        }

        #endregion


    }
}
