﻿namespace AdtomaticLite
{
    partial class FrLogs
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrLogs));
            this.pnlInf = new System.Windows.Forms.Panel();
            this.btnClose = new System.Windows.Forms.Button();
            this.olvLogs = new BrightIdeasSoftware.ObjectListView();
            this.olvcFechaProg = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.olvcFechaInicio = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.olvcFechaFinPub = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.olvcCodigoResultado = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.olvcDescResult = ((BrightIdeasSoftware.OLVColumn)(new BrightIdeasSoftware.OLVColumn()));
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.tsbDetalle = new System.Windows.Forms.ToolStripButton();
            this.pnlInf.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.olvLogs)).BeginInit();
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlInf
            // 
            this.pnlInf.Controls.Add(this.btnClose);
            this.pnlInf.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnlInf.Location = new System.Drawing.Point(0, 335);
            this.pnlInf.Name = "pnlInf";
            this.pnlInf.Size = new System.Drawing.Size(785, 36);
            this.pnlInf.TabIndex = 0;
            // 
            // btnClose
            // 
            this.btnClose.Location = new System.Drawing.Point(698, 6);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(75, 23);
            this.btnClose.TabIndex = 0;
            this.btnClose.Text = "Cerrar";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // olvLogs
            // 
            this.olvLogs.AllColumns.Add(this.olvcFechaProg);
            this.olvLogs.AllColumns.Add(this.olvcFechaInicio);
            this.olvLogs.AllColumns.Add(this.olvcFechaFinPub);
            this.olvLogs.AllColumns.Add(this.olvcCodigoResultado);
            this.olvLogs.AllColumns.Add(this.olvcDescResult);
            this.olvLogs.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.olvcFechaProg,
            this.olvcFechaInicio,
            this.olvcFechaFinPub,
            this.olvcCodigoResultado,
            this.olvcDescResult});
            this.olvLogs.Dock = System.Windows.Forms.DockStyle.Fill;
            this.olvLogs.GridLines = true;
            this.olvLogs.Location = new System.Drawing.Point(0, 25);
            this.olvLogs.Name = "olvLogs";
            this.olvLogs.ShowGroups = false;
            this.olvLogs.Size = new System.Drawing.Size(785, 310);
            this.olvLogs.Sorting = System.Windows.Forms.SortOrder.Descending;
            this.olvLogs.TabIndex = 1;
            this.olvLogs.UseCompatibleStateImageBehavior = false;
            this.olvLogs.View = System.Windows.Forms.View.Details;
            // 
            // olvcFechaProg
            // 
            this.olvcFechaProg.AspectName = "FechaProgramada";
            this.olvcFechaProg.Text = "Fecha Programada";
            this.olvcFechaProg.Width = 121;
            // 
            // olvcFechaInicio
            // 
            this.olvcFechaInicio.AspectName = "FechaInicio";
            this.olvcFechaInicio.Text = "Fecha inicio publicación";
            this.olvcFechaInicio.Width = 138;
            // 
            // olvcFechaFinPub
            // 
            this.olvcFechaFinPub.AspectName = "FechaFin";
            this.olvcFechaFinPub.Text = "Fecha fin publicación";
            this.olvcFechaFinPub.Width = 131;
            // 
            // olvcCodigoResultado
            // 
            this.olvcCodigoResultado.AspectName = "CodigoResultado";
            this.olvcCodigoResultado.Text = "Codigo resultado";
            this.olvcCodigoResultado.Width = 98;
            // 
            // olvcDescResult
            // 
            this.olvcDescResult.AspectName = "DescResult";
            this.olvcDescResult.Text = "Descripción resultado";
            this.olvcDescResult.Width = 269;
            // 
            // toolStrip1
            // 
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsbDetalle});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(785, 25);
            this.toolStrip1.TabIndex = 2;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // tsbDetalle
            // 
            this.tsbDetalle.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.tsbDetalle.Image = ((System.Drawing.Image)(resources.GetObject("tsbDetalle.Image")));
            this.tsbDetalle.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbDetalle.Name = "tsbDetalle";
            this.tsbDetalle.Size = new System.Drawing.Size(23, 22);
            this.tsbDetalle.Text = "Ver detalle";
            this.tsbDetalle.Click += new System.EventHandler(this.tsbDetalle_Click);
            // 
            // FrLogs
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(785, 371);
            this.Controls.Add(this.olvLogs);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.pnlInf);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FrLogs";
            this.Text = "Logs";
            this.pnlInf.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.olvLogs)).EndInit();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel pnlInf;
        private BrightIdeasSoftware.ObjectListView olvLogs;
        private BrightIdeasSoftware.OLVColumn olvcFechaProg;
        private BrightIdeasSoftware.OLVColumn olvcFechaInicio;
        private BrightIdeasSoftware.OLVColumn olvcFechaFinPub;
        private BrightIdeasSoftware.OLVColumn olvcCodigoResultado;
        private BrightIdeasSoftware.OLVColumn olvcDescResult;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton tsbDetalle;
        private System.Windows.Forms.Button btnClose;
    }
}