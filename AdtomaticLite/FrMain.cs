﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Windows.Forms;
using WatiN.Core;
namespace AdtomaticLite
{
    public partial class FrMain : System.Windows.Forms.Form
    {
        private List<Entities.Anuncio> _anunciosList;
        private Queue<Entities.Anuncio> _queue = new Queue<Entities.Anuncio>();
        List<Entities.Anuncio> _anuncios = new List<Entities.Anuncio>();
        List<Entities.Anuncio> _anunciosAutorenueva = new List<Entities.Anuncio>();
        List<Entities.Anuncio> _anunciosJuntado = new List<Entities.Anuncio>();
        List<Entities.Anuncio> _anunciosJuntadoAutorenueva = new List<Entities.Anuncio>();
        private int intervaloInterfaz;
        private bool _running;
        private bool inicio = true;
        private Entities.Anuncio _currentAnuncio;
        private System.Threading.Timer _timer;
        private bool _stopped = true;
        protected Browser _browser;
        public FrMain()
        {
            InitializeComponent();

        }

        #region Common

        private void Initialize()
        {
            CheckCliente();
            InitializeTimer();
            RunTimer();
            olvAnuncios.Sort(olvColumn2, SortOrder.Ascending);

        }

        private void CheckCliente()
        {
            if (Business.Configuration.IdCliente.Equals(-1))
            {
                FrConfigCliente frConfigCli = new FrConfigCliente();
                frConfigCli.ShowDialog();
            }
            if (!Business.Cliente.IsActivo(Business.Configuration.IdCliente))
            {
                StopTimer();
                timerCheckCliente.Stop();
                MessageBox.Show("El cliente esta desactivado", "Adtomatic Lite");
                Application.Exit();
            }
        }

        private List<Entities.Anuncio> GetAnunciosList()
        {
            if (_anunciosList == null)
                _anunciosList = Business.Anuncios.GetAnunciosActivos(Business.Configuration.IdCliente);
            _anunciosList = _anunciosList.OrderBy(anuncio => anuncio.NextRun).ToList();

            return _anunciosList;
        }

        private void Excecute(object obj)
        {
            try
            {
                if (_running)
                    return;

                _running = true;

                if (_queue.Count == 0)
                {
                    UpdateQueue();
                }
                if (_queue.Count == 0)
                {
                    _running = false;
                    SetDescPublicando("Inactivo. Esperando anuncios ...", System.Drawing.Color.Red);
                    return;
                }

                _currentAnuncio = _queue.Dequeue();

                if (_currentAnuncio.activo == "activo")
                {
                    
                    SetDescPublicando(string.Format("Publicando anuncio {0}", _currentAnuncio.Name), System.Drawing.Color.Green);
                    Business.Run run = new Business.Run();
                    int si=_anunciosJuntado.Count;
                    run.RunAd(_currentAnuncio);
                    si = _anunciosJuntado.Count;
                    if (_currentAnuncio.IntervaloRenovPago != 0)
                        UpdateNextRunComienzo(_currentAnuncio);
                    if (_currentAnuncio.Tipo != Entities.Enum.eTablones.MilAnunciosRenovPago)
                    {
                        _currentAnuncio.aumentarPuntero();
                        _currentAnuncio.NextRun = _currentAnuncio.nextRunNuevo();
                        _anuncios.OrderBy(anuncio => anuncio.NextRun);
                        //olvAnuncios.SetObjects(_anuncios);
                    }
                    else
                    {
                        _currentAnuncio.NextRun = _currentAnuncio.CalculateNextRunDate();
                        _anunciosAutorenueva.OrderBy(anuncio => anuncio.NextRun);
                        //olvAnunciosAutorenueva.SetObjects(_anunciosAutorenueva);
                    }
                    si = _anunciosJuntado.Count;
                    if (_currentAnuncio.Tipo == Entities.Enum.eTablones.MilAnunciosRenovPago)
                        setCurrentAnuncioAAnunciosAutorenueva();
                    setCurrentAnuncioAAnuncios(_currentAnuncio);
                    olvAnuncios.SetObjects(_anunciosJuntado);
                    LoadAdvertisements(true);
                    UpdateQueue();
                    _running = false;
                    SetDescPublicando("Inactivo. Esperando anuncios ...", System.Drawing.Color.Red);
                }

            }
            catch (Exception)
            {
                _running = false;
                SetDescPublicando("Inactivo. Esperando anuncios ...", System.Drawing.Color.Red);
            }
        }
        private void UpdateNextRunComienzo(Entities.Anuncio anuComienzo) {
            int aux = (int)anuComienzo.IntervaloRenovPago;
            anuComienzo.IntervaloRenovPago = 0;
            anuComienzo.NextRun = anuComienzo.CalculateNextRunDate();
            anuComienzo.IntervaloRenovPago = aux;
        }
        private void setCurrentAnuncioAAnunciosAutorenueva() {
            foreach (Entities.Anuncio anu in _anunciosJuntadoAutorenueva) {
                if (anu.Mail == _currentAnuncio.Mail)
                    anu.Logs = _currentAnuncio.Logs;
            }
            olvAnunciosAutorenueva.SetObjects(_anunciosJuntadoAutorenueva);
        }
        private void setCurrentAnuncioAAnuncios(Entities.Anuncio currentAnuncio)
        {
            _anuncios = _anuncios.OrderBy(anuncio => anuncio.NextRun).ToList();
            int i = 0;

            foreach (Entities.Anuncio anuncio in _anunciosJuntado)
            {
                if (anuncio.Name == currentAnuncio.Name)
                {
                    foreach (Entities.Anuncio an in _anuncios)
                    {
                        if (an.Name == currentAnuncio.Name)
                        {
                            Entities.Anuncio anuncioInsertar = new Entities.Anuncio();
                            anuncioInsertar.NextRun = an.NextRun;
                            anuncioInsertar.Mail = currentAnuncio.Mail;
                            anuncioInsertar.Password = currentAnuncio.Password;
                            anuncioInsertar.Logs = currentAnuncio.Logs;
                            anuncioInsertar.LastRun = currentAnuncio.LastRun;
                            anuncioInsertar.Name = currentAnuncio.Name;
                            anuncioInsertar.check = currentAnuncio.check;
                            anuncioInsertar.Tipo = currentAnuncio.Tipo;
                            anuncioInsertar.activo = currentAnuncio.activo;
                            _anunciosJuntado[i] = anuncioInsertar;
                            //_anunciosJuntado[i].NextRun = 
                            break;
                        }
                    }
                    break;
                }
                i++;
            }
        }
        private void UpdateQueue()
        {
            //LoadAdvertisements(false);
            _queue.Clear();
            foreach (Entities.Anuncio anuncio in _anuncios)
            {
                if (anuncio.NextRun != null && anuncio.NextRun.Value <= DateTime.Now)
                {
                    if (anuncio.activo == "activo")
                        _queue.Enqueue(anuncio);
                }
            }
            foreach (Entities.Anuncio anuncio in _anunciosAutorenueva)
            {
                if (anuncio.NextRun != null && anuncio.NextRun.Value <= DateTime.Now)
                {
                    if (anuncio.activo == "activo")
                    {
                        if (anuncio.Name.Contains("Comienzo"))
                        {
                            if (tiempoValido(anuncio))
                                _queue.Enqueue(anuncio);
                        }
                        else
                        {
                            _queue.Enqueue(anuncio);
                        }
                    }
                }
            }
        }
        private bool tiempoValido(Entities.Anuncio anuncio) {
            bool res = false;
            foreach (Entities.Anuncio anu in _anunciosAutorenueva) {
                if (anu.Mail == anuncio.Mail && anu.Name.Contains("Final")) {
                    if (((DateTime)anu.NextRun).TimeOfDay >= DateTime.Now.TimeOfDay)
                        res = true;
                    else
                    {
                        UpdateNextRunComienzo(anuncio);
                        res = false;
                    }
                }
            }
                
            return res;
        }

        private void LoadAdvertisements(bool updateNextRun)
        {
            // Obtengo anuncios Activos.
            _anuncios = new List<Entities.Anuncio>();
            //_anunciosJuntado = new List<Entities.Anuncio>(); 
            _anunciosAutorenueva = new List<Entities.Anuncio>();
            foreach (Entities.Anuncio anuncio in GetAnunciosList())
            {
                if (updateNextRun)
                {
                    // Actualizo hora ejecucion proxima corrida.
                    if (anuncio.Tipo == Entities.Enum.eTablones.MilAnunciosRenovPago)
                        anuncio.NextRun = anuncio.CalculateNextRunDate();
                    else
                        anuncio.NextRun = anuncio.nextRunNuevo(); ;
                }
                //if (anuncio.NextRun != null)
                //{
                    if (anuncio.Tipo == Entities.Enum.eTablones.MilAnunciosRenovPago)
                    {
                        if (!inicio)
                            if (_currentAnuncio.Tipo == Entities.Enum.eTablones.MilAnunciosRenovPago)
                                if (_currentAnuncio.Mail == anuncio.Mail)
                                {
                                    if ((_currentAnuncio.Name.Contains("Comienzo") && anuncio.Name.Contains("Comienzo")))
                                    {
                                        anuncio.activo = _currentAnuncio.activo;
                                        anuncio.NextRun = _currentAnuncio.NextRun;
                                    }

                                    if ((_currentAnuncio.Name.Contains("Final") && anuncio.Name.Contains("Final")))
                                    {
                                        anuncio.activo = _currentAnuncio.activo;
                                        anuncio.NextRun = _currentAnuncio.NextRun;
                                    }
                                }
                        _anunciosAutorenueva.Add(anuncio);
                    }
                    else
                    {
                        _anuncios.Add(anuncio);
                        if (!existe(anuncio, _anunciosJuntado))
                        {
                            Entities.Anuncio anuncioInsertar = new Entities.Anuncio();
                            anuncioInsertar.NextRun = anuncio.NextRun;
                            anuncioInsertar.Mail = anuncio.Mail;
                            anuncioInsertar.Password = anuncio.Password;
                            anuncioInsertar.Logs = anuncio.Logs;
                            anuncioInsertar.LastRun = anuncio.LastRun;
                            anuncioInsertar.Name = anuncio.Name;
                            anuncioInsertar.check = anuncio.check;
                            anuncioInsertar.Tipo = anuncio.Tipo;
                            anuncioInsertar.activo = "inactivo";
                            _anunciosJuntado.Add(anuncioInsertar);
                        }

                        // else
                        //       addSiguiente(anuncio, _anunciosJuntado);
                    }
                //}
                //else
                //{
                    //advertisement.Active = false;
                    //mAdvertisementsBL.UpdateAdvertisement(advertisement);
                    //AdvertisementUpdated(this, new EventArgs());
                //}
            }
            _anuncios.OrderBy(anuncio => anuncio.NextRun);
            if (inicio)
            {
                olvAnuncios.SetObjects(_anunciosJuntado);
                inicio = false;
                ajustarListaParaTablaPago();
                olvAnunciosAutorenueva.SetObjects(_anunciosJuntadoAutorenueva);
            }           
        }

        private void ajustarListaParaTablaPago() {
            List<Entities.Anuncio> _anunciosJuntadoAutorenuevasAuxilliar = _anunciosAutorenueva;
            _anunciosJuntadoAutorenuevasAuxilliar = _anunciosJuntadoAutorenuevasAuxilliar.OrderBy(anuncio => anuncio.Mail).ToList();
            int count = 0;
            Entities.Anuncio Anuncio1= null;
            Entities.Anuncio Anuncio2 = null;
            foreach (Entities.Anuncio anuncio in _anunciosJuntadoAutorenuevasAuxilliar)
            {

                if (count == 0){
                    Anuncio1 = anuncio;
                    count++;
                }
                    
                else
                {
                    Anuncio2 = anuncio;
                    generarAnuncioDePagoParaTabla(Anuncio1,Anuncio2);
                    count = 0;
                } 
                

            }
        }

        private void generarAnuncioDePagoParaTabla(Entities.Anuncio anuncio1, Entities.Anuncio anuncio2)
        {
            Entities.Anuncio anuncio = new Entities.Anuncio();
            anuncio = anuncio1.Clone();
            if (anuncio1.Name.Contains("final"))
            {
                anuncio.endDefault = ((DateTime)anuncio1.StartTime).TimeOfDay;
                anuncio.startDefault = ((DateTime)anuncio2.StartTime).TimeOfDay;
            }else{
                anuncio.endDefault = ((DateTime)anuncio2.StartTime).TimeOfDay;
                anuncio.startDefault = ((DateTime)anuncio1.StartTime).TimeOfDay;
            }
            anuncio.Name = (anuncio.Name.Split('-'))[0];
            _anunciosJuntadoAutorenueva.Add(anuncio);
        }

        private void addSiguiente(Entities.Anuncio anuncio, List<Entities.Anuncio> anunciosJuntado)
        {
            int i = 0;
            foreach (Entities.Anuncio anu in anunciosJuntado)
            {
                if (anu.Name == anuncio.Name)
                {
                    if (anu.NextRun > anuncio.NextRun)
                    {
                        anuncio.LastRun = anu.LastRun;
                        anuncio.Logs = anu.Logs;
                        anunciosJuntado[i] = anuncio;
                        //anunciosJuntado.Add(anuncio);
                        break;
                    }
                }
                i++;
            }
        }

        private bool existe(Entities.Anuncio anuncio, List<Entities.Anuncio> anunciosJuntado)
        {
            Entities.Anuncio anuncioRes = null;
            //Entities.Anuncio anuncioAux = null;
            //_anuncios.OrderBy(anuncio => anuncio.NextRun);
            bool res = false;
            foreach (Entities.Anuncio anu in anunciosJuntado)
            {
                if (anu.Name == anuncio.Name)
                {
                    res = true;
                    break;
                }
            }
            return res;
        }

        private void SetDescPublicando(string text, System.Drawing.Color color)
        {
            if (lblEstadoDesc.InvokeRequired)
            {
                lblEstadoDesc.Invoke(new MethodInvoker(delegate
                {
                    lblEstadoDesc.Text = text;
                    lblEstadoDesc.ForeColor = color;
                }));
            }
        }

        private void SyncWithWAC()
        {
            _anunciosList = Business.Anuncios.GetAnunciosActivos(Business.Configuration.IdCliente);
        }

        private void ManageTray()
        {
            if (FormWindowState.Minimized == this.WindowState)
            {
                trayIcon.Visible = true;
                trayIcon.ShowBalloonTip(500);
                this.Hide();
            }

            else if (FormWindowState.Normal == this.WindowState)
            {
                trayIcon.Visible = false;
            }
        }

        private void ConfigureAvailableActions()
        {
            if (olvAnuncios.SelectedObject == null)
            {
                tsbActivate.Enabled = false;
                tsbDeActivate.Enabled = false;
                recargarSaldo.Enabled = false;
                editarPublicacion.Enabled = false;
                return;
            }
            editarPublicacion.Enabled = true;
            Entities.Anuncio selectedAnuncio = (Entities.Anuncio)olvAnuncios.SelectedObject;
            tsbActivate.Enabled = ((selectedAnuncio.Tipo == Entities.Enum.eTablones.MilAnunciosRenovPago) && (_stopped));
            tsbDeActivate.Enabled = ((selectedAnuncio.Tipo == Entities.Enum.eTablones.MilAnunciosRenovPago) && (_stopped));
            recargarSaldo.Enabled = ((selectedAnuncio.Tipo == Entities.Enum.eTablones.MilAnunciosRenovPago));
        }

        private void ActivateAd()
        {
            if (SelectedAd == null)
                return;
            _running = true;
            Business.Run run = new Business.Run();
            run.RunAd(SelectedAd);
            _running = false;
        }

        private void DeactivateAd()
        {
            if (SelectedAd == null)
                return;
            _running = true;
            Business.Run run = new Business.Run();
            Entities.Anuncio anuncio = SelectedAd.Clone();
            anuncio.IntervaloRenovPago = 0;
            anuncio.RenovarNocturno = 0;
            run.RunAd(anuncio);
            _running = false;
        }

        #endregion

        #region Timer

        private void InitializeTimer()
        {
            TimerCallback timerCallback = new TimerCallback(Excecute);
            _timer = new System.Threading.Timer(timerCallback, null, System.Threading.Timeout.Infinite, Common.TIMER_PERIOD);
        }

        private void RunTimer()
        {
            LoadAdvertisements(true);
            _timer.Change(0, Common.TIMER_PERIOD);
            olvAnuncios.Sort(olvColumn2, SortOrder.Ascending);
            //tsbPlay.Enabled = false;
            //tsbStop.Enabled = true;
            tsbRefresh.Enabled = false;
            _stopped = false;
        }

        protected void InteractivePause(int seconds)
        {
            TimeSpan length = new TimeSpan(0, 0, seconds);
            DateTime start = DateTime.Now;
            TimeSpan restTime = new TimeSpan(200000); // 20 milliseconds
            while (true)
            {
                System.Windows.Forms.Application.DoEvents();
                TimeSpan remainingTime = start.Add(length).Subtract(DateTime.Now);
                if (remainingTime > restTime)
                {
                    //System.Diagnostics.Debug.WriteLine(string.Format("1: {0}", remainingTime));
                    // Wait an insignificant amount of time so that the
                    // CPU usage doesn't hit the roof while we wait.
                    System.Threading.Thread.Sleep(restTime);
                }
                else
                {
                    //System.Diagnostics.Debug.WriteLine(string.Format("2: {0}", remainingTime));
                    if (remainingTime.Ticks > 0)
                        System.Threading.Thread.Sleep(remainingTime);
                    break;
                }
            }

        }

        private void StopTimer()
        {/*
            if (_timer != null)
                _timer.Change(Timeout.Infinite, Timeout.Infinite);
            tsbStop.Enabled = false;
            tsbPlay.Enabled = true;
            tsbRefresh.Enabled = true;
            _stopped = true;
          */
        }

        #endregion

        #region Events

        private void FrMain_Load(object sender, EventArgs e)
        {
            Initialize();
        }

        private void tsbPlay_Click(object sender, EventArgs e)
        {
            //RunTimer();
        }

        private void tsbStop_Click(object sender, EventArgs e)
        {
            StopTimer();
        }

        private void tsbRefresh_Click(object sender, EventArgs e)
        {
            SyncWithWAC();
        }

        private void FrMain_Resize(object sender, EventArgs e)
        {
            ManageTray();
        }

        private void trayIcon_DoubleClick(object sender, EventArgs e)
        {
            this.Show();
        }

        private void timerCheckCliente_Tick(object sender, EventArgs e)
        {
            CheckCliente();
        }

        private void tsbActivate_Click(object sender, EventArgs e)
        {
            ActivateAd();
            //RunTimer();
        }

        private void tsbDeActivate_Click(object sender, EventArgs e)
        {
            DeactivateAd();
        }

        private void olvAnuncios_SelectedIndexChanged(object sender, EventArgs e)
        {
            ConfigureAvailableActions();
        }

        private void tsbLogs_Click(object sender, EventArgs e)
        {
            Entities.Anuncio anuncio = SelectedAd;
            if (SelectedAd == null)
                return;
            FrLogs frlogs = new FrLogs();
            frlogs.Initialize(anuncio.Logs);
            frlogs.ShowDialog();
        }

        private void FrMain_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.StopTimer();
            this.Hide();
            this.RunOffAds();
        }

        private void refressh_Click(object sender, EventArgs e)
        {
            try
            {
                cargarSaldos();
            }
            catch (Exception ew)
            {

            }

        }

        /* private void recargarSaldo_Click(object sender, EventArgs e)
         {
             if (olvAnuncios.SelectedObject == null)
             {
                 MessageBox.Show("debe seleccionar una publicacion");
                 return;
             }
             _browser = new IE(true);
             string url = "http://www.milanuncios.com/mis-anuncios/?recargar=1";
             _browser.GoTo(url);
             Entities.Anuncio selectedAnuncio = (Entities.Anuncio)olvAnuncios.SelectedObject;
             SetLogin(selectedAnuncio.Mail, selectedAnuncio.Password);
             Submit();
         }*/

        /*private void editarPublicacion_Click(object sender, EventArgs e)
        {
            if (olvAnuncios.SelectedObject == null)
            {
                MessageBox.Show("debe seleccionar una publicacion");
                return;
            }
            _browser = new IE(true);
            string url = "http://www.milanuncios.com/mis-anuncios/";
            _browser.GoTo(url);
            Entities.Anuncio selectedAnuncio = (Entities.Anuncio)olvAnuncios.SelectedObject;
            SetLogin(selectedAnuncio.Mail, selectedAnuncio.Password);
            Submit();

            //InteractivePause(2);
            //hacerTap(18);
            //enter();


        }*/

        private void tsbPlay_Click_1(object sender, EventArgs e)
        {
            /*if (habilitarIntervaloEntreAnuncios())
            {
                habilitarIntervaloEntreAnuncios(true);
                tbIntervaloEntreAnuncios.Text = "2";
                tbIntervaloEntreAnuncios.Focus();
            }
            else*/
            if (IntervaloValido())
                    updateCkecksGratuitos(true);
        }

        private bool IntervaloValido() {
            bool res = false;
            try
            {
                 intervaloInterfaz = int.Parse(tbIntervaloEntreAnuncios.Text);
                res = true;
            }
            catch (Exception ex) {
                MessageBox.Show("intervalo erroneo, debe ser un numero entero");
                tbIntervaloEntreAnuncios.Focus();
            }
            return res;
        }

        private bool habilitarIntervaloEntreAnuncios()
        {
            bool res = false;
            int contador = 0;
            foreach (object o in olvAnuncios.Objects)
            {
                Entities.Anuncio selectedAnuncio = (Entities.Anuncio)o;
                if (selectedAnuncio.check)
                    contador++;
            }
            if (contador > 1 && !checkBoxSeleccionarTodos.Checked)
                if (MessageBox.Show("Desea colocar un intervalo entre los anuncios?", "Confirm delete", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    res = true;
                }
            return res;   
        }
        private void checkBoxSeleccionarTodos_CheckedChanged(object sender, EventArgs e)
        {
            List<Entities.Anuncio> anunciosJuntos = new List<Entities.Anuncio>();
            bool var = checkBoxSeleccionarTodos.Checked;
            foreach (object o in olvAnuncios.Objects)
            {
                Entities.Anuncio selectedAnuncio = (Entities.Anuncio)o;
                selectedAnuncio.check = var;
                anunciosJuntos.Add(selectedAnuncio);
            }
            olvAnuncios.SetObjects(anunciosJuntos);
           // habilitarIntervaloEntreAnuncios(var);
        }
        private void habilitarIntervaloEntreAnuncios(bool habilitar){
            tbIntervaloEntreAnuncios.Enabled = habilitar;
            NameIntervaloEntreAnuncios.Enabled = habilitar;
        }
        private void checkBoxSeleccionarTodosAutorenueva_CheckedChanged(object sender, EventArgs e)
        {
            List<Entities.Anuncio> anunciosJuntosPagado = new List<Entities.Anuncio>();
            bool var = checkBoxSeleccionarTodosAutorenueva.Checked;
            foreach (object o in olvAnunciosAutorenueva.Objects)
            {
                Entities.Anuncio selectedAnuncio = (Entities.Anuncio)o;
                selectedAnuncio.check = var;
                anunciosJuntosPagado.Add(selectedAnuncio);
            }
            olvAnunciosAutorenueva.SetObjects(anunciosJuntosPagado);
        }

        #endregion

        [STAThread]
        private void RunOffAds()
        {
            if (_anunciosAutorenueva == null)
                return;
            List<Entities.Anuncio> anunciosOff = new List<Entities.Anuncio>();
            foreach (Entities.Anuncio anuncio in _anunciosAutorenueva)
            {
                if ((anuncio.Tipo == Entities.Enum.eTablones.MilAnunciosRenovPago) && (anuncio.IntervaloRenovPago == 0))
                    anunciosOff.Add(anuncio);
            }
            if (anunciosOff.Count == 0)
                return;
            FrOffShutdown frOffShutDown = new FrOffShutdown();
            frOffShutDown.Initialize(anunciosOff);
            frOffShutDown.RunOffAds();
            frOffShutDown.ShowDialog();
        }

        #region Properties

        public Entities.Anuncio SelectedAd
        {
            get
            {
                if (olvAnuncios.SelectedObject == null)
                    return null;
                return (Entities.Anuncio)olvAnuncios.SelectedObject;
            }
        }
        public Entities.Anuncio SelectedAdAutorenueva
        {
            get
            {
                if (olvAnunciosAutorenueva.SelectedObject == null)
                    return null;
                return (Entities.Anuncio)olvAnunciosAutorenueva.SelectedObject;
            }
        }
        #endregion

        #region RefrescarSaldos


        private void hacerTap(int cantidad)
        {
            for (int i = 0; i < cantidad; i++)
            {
                System.Windows.Forms.SendKeys.SendWait("{TAB}");
                InteractivePause(1);
            }

        }
        private void enter()
        {
            System.Windows.Forms.SendKeys.SendWait("{ENTER}");
        }
        private void escribir(String texto)
        {
            System.Windows.Forms.SendKeys.SendWait(texto);
        }
        private void cargarSaldos()
        {
            string mail = "";
            string pass = "";
            List<Entities.Anuncio> anuncios = new List<Entities.Anuncio>();

            foreach (object o in olvAnunciosAutorenueva.Objects)
            {
                Entities.Anuncio selectedAnuncio = (Entities.Anuncio)o;
                mail = selectedAnuncio.Mail;
                pass = selectedAnuncio.Password;
                if (mail != "")
                {
                    _browser = new IE(true);
                    getPagina(false, _browser, "http://www.milanuncios.com/mis-anuncios/");
                    SetLogin(mail, pass, _browser);
                    Submit(_browser);
                    string saldo = getSaldo(_browser);
                    try
                    {
                        selectedAnuncio.check = true;
                        int enteroSaldo = int.Parse(saldo);
                        selectedAnuncio.Saldo = enteroSaldo + "";
                        anuncios.Add(selectedAnuncio);
                    }
                    catch (Exception e)
                    {

                    }
                    _browser.Close();
                }

            }
            olvAnunciosAutorenueva.SetObjects(anuncios);
        }
        protected void getPagina(Boolean visivilidad, Browser browser, string url)
        {
            Settings.MakeNewIeInstanceVisible = visivilidad;

            //string url = "http://www.milanuncios.com/mis-anuncios/";
            browser.GoTo(url);
            browser.WaitForComplete();
        }
        protected void SetLoginSimple(string mail, string pass, Browser browser)
        {
            string NAME_MAIL = "email";
            string NAME_PASS = "contra";

            TextField tfName = browser.TextField(NAME_MAIL);
            tfName.Value = mail;
            TextField tfPass = browser.TextField(NAME_PASS);
            tfPass.Value = pass;
        }
        protected void SetLogin(string mail, string pass, Browser browser)
        {
            string NAME_MAIL = "email";
            string NAME_PASS = "contra";

            Image pagoImage = FindImage(browser, "icoSalir.gif");
            if (pagoImage != null)
            {

                pagoImage.Click();
                browser.WaitForComplete();
            }
            TextField tfName = browser.TextField(NAME_MAIL);
            tfName.Value = mail;
            TextField tfPass = browser.TextField(NAME_PASS);
            tfPass.Value = pass;
        }

        protected void Submit(Browser browser)
        {
            foreach (WatiN.Core.Button wButton in browser.Forms[0].Buttons)
            {
                if (wButton.Value.Equals("ENVIAR >>"))
                {
                    wButton.Click();
                    break;
                }
            }
            browser.WaitForComplete();
        }
        private string getSaldo(Browser browser)
        {
            InteractivePause(2);
            int pos = browser.Body.OuterHtml.IndexOf("='#cec'") + 9;
            string html = browser.Body.OuterHtml;
            string saldoText = html.Substring(pos, 100);
            string[] split = saldoText.Split('<');
            string a = split[0];
            return a;
        }
        private Image FindImage(Browser browser, string nombre)
        {
            foreach (Image wImage in browser.Images)
            {
                if (wImage.Src.Contains(nombre))
                    return wImage;
            }
            return null;
        }
        private Link FindHref(Browser browser, string nombre)
        {
            foreach (Link wLink in browser.Links)
            {
                if (wLink.Url == nombre)
                    return wLink;
            }
            return null;
        }
        /*
        private Image FindPagoImage(Browser browser)
        {
            foreach (Image wImage in browser.Images)
            {
                if (wImage.Src.Contains("tarjetas.png"))
                    return wImage;
            }
            return null;
        }
        */
        #endregion

        private void toolStripButton2_Click(object sender, EventArgs e)
        {

        }

        private void tsbStop_Click_1(object sender, EventArgs e)
        {
            updateCkecksGratuitos(false);
        }


        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void updateCkecksGratuitos(bool activar)
        {           
            List<Entities.Anuncio> anuncios = new List<Entities.Anuncio>();
            List<Entities.Anuncio> anunciosJuntos = new List<Entities.Anuncio>();
            int contadorActivados = 0;
            foreach (object o in olvAnuncios.Objects)
            {
                Entities.Anuncio selectedAnuncio = (Entities.Anuncio)o;
                seleccionaTodosDelMismoAnuncioYAgregalos(selectedAnuncio, anuncios, activar,contadorActivados); 
                if (activar && selectedAnuncio.check)
                    contadorActivados++;
                
                if (selectedAnuncio.check)
                    if (activar)
                        selectedAnuncio.activo = "activo";
                    else
                        selectedAnuncio.activo = "inactivo";
                anunciosJuntos.Add(selectedAnuncio);
            }
            calcularNextRuns(anuncios);
            _anuncios = anuncios;
            ponerAlMasCercanoATabla(anunciosJuntos);
            _anunciosJuntado = anunciosJuntos;
            olvAnuncios.SetObjects(anunciosJuntos);
            
            UpdateQueue();
            //string testo = olvColumn1.ListView.GetItemAt(0, 2).Name;      //ForeColor = System.Drawing.Color.Red;
        }

        private void ponerAlMasCercanoATabla(List<Entities.Anuncio> anunciosJuntado)
        {
            _anuncios = _anuncios.OrderBy(ani => ani.NextRun).ToList();

            for (int i = 0; i < anunciosJuntado.Count; i++ )
            {

                foreach (Entities.Anuncio ani in _anuncios)
                {
                    if (ani.Name == anunciosJuntado[i].Name)
                    {
                        Entities.Anuncio anuAux = new Entities.Anuncio();
                        anuAux = ani.Clone();
                        anuAux.Logs = anunciosJuntado[i].Logs;
                        anuAux.LastRun = anunciosJuntado[i].LastRun;
                        anunciosJuntado[i] = anuAux;
                        break;
                    }
                }
            }
            /*foreach (Entities.Anuncio anu in _anuncios)
            {
                if (anu.activo == "activo")
                    if (anu.NextRun != null)
                    {
                        int i = 0;
                        foreach (Entities.Anuncio ani in anunciosJuntado)
                        {
                            if (ani.Name == anu.Name)
                            {
                                anunciosJuntado[i] = anu;
                                break;
                            }
                            i++;
                        }
                        
                    }
            }*/
            //olvAnuncios.SetObjects(_anunciosJuntado);
        }
        private void calcularNextRuns(List<Entities.Anuncio> anuncios){
            foreach (Entities.Anuncio anu in anuncios)
            {
                if (anu.activo == "activo")
                    anu.NextRun = anu.nextRunNuevo();
            }
            
        }

        private void seleccionaTodosDelMismoAnuncioYAgregalos(Entities.Anuncio anuncio, List<Entities.Anuncio> anuncios, bool activar, int contadorActivados)
        {
            bool checkeado = anuncio.check;
            _anuncios = _anuncios.OrderBy( ani => ani.Name).ToList();
            List<Entities.Anuncio> anunciosPorNombre = new List<Entities.Anuncio>();
            foreach (Entities.Anuncio anu in _anuncios)
            {
                if (anuncio.Name == anu.Name)
                {
                    if (checkeado)
                        if (activar)
                        {
                            anu.activo = "activo";
                            anunciosPorNombre.Add(anu);
                        }
                        else
                        {
                            anu.activo = "inactivo";
                            anu.horasDeSalida = new List<DateTime>() ;
                            anu.punteroHoraSalida = 0;
                        }
                    anu.check = checkeado;
                    anuncios.Add(anu);
                    
                }
            }
            if (activar && checkeado) {
                
                generarHorariosMail(anunciosPorNombre, (intervaloInterfaz*contadorActivados));
            }
            

        }

        private void updateCkecksAutorenueva(bool activar)
        {
            List<Entities.Anuncio> anunciosAutorenueva = new List<Entities.Anuncio>();
            foreach (object o in olvAnunciosAutorenueva.Objects)
            {
                Entities.Anuncio selectedAnuncio = (Entities.Anuncio)o;
                if (selectedAnuncio.check)
                {
                    if (activar)
                    {
                        selectedAnuncio.activo = "activo";
                    }else{
                        selectedAnuncio.activo = "inactivo";
                    }
                    activarDesactivarAnunciosPago(selectedAnuncio, activar);
                }
                anunciosAutorenueva.Add(selectedAnuncio);
            }

            olvAnunciosAutorenueva.SetObjects(anunciosAutorenueva);
            //_anunciosAutorenueva = anunciosAutorenueva;
            UpdateQueue();
        }

        private void activarDesactivarAnunciosPago(Entities.Anuncio anuncio, bool activar) {
            foreach (Entities.Anuncio anu in _anunciosAutorenueva) {
                if (anu.Mail == anuncio.Mail)
                    if (activar)
                    {
                        anu.activo = "activo";
                        if (((DateTime)anu.NextRun).Day == DateTime.Today.AddDays(1).Day)
                        {
                            Business.Run run = new Business.Run();
                            run.RunAd(anu);
                        }
                    }else{
                        if (anu.Name.Contains("Final"))
                        {
                            Business.Run run = new Business.Run();
                            run.RunAd(anu);
                        }
                        anu.activo = "inactivo";
                    }
            }
        }
        private void refressh_Click_1(object sender, EventArgs e)
        {
            LoadAdvertisements(true);
            UpdateQueue();
        }

        private void toolStripButton7_Click(object sender, EventArgs e)
        {
            //LoadAdvertisements(true);
            //UpdateQueue();
            cargarSaldos();
        }

        private void tsmiRestore_Click(object sender, EventArgs e)
        {
            this.Show();
            trayIcon.ShowBalloonTip(1000);
            this.WindowState = FormWindowState.Normal;
            trayIcon.Visible = false;
        }

        private void olvAnuncios_SelectedIndexChanged_1(object sender, EventArgs e)
        {

        }

        private void tsbLogs_Click_1(object sender, EventArgs e)
        {
            Entities.Anuncio anuncioSeleccionado = SelectedAd;
            if (SelectedAd == null)
                return;
            FrLogs frlogs = new FrLogs();
            List<Entities.Log> logs = new List<Entities.Log>();
            foreach (Entities.Anuncio anu in _anuncios)
            {
                if (anuncioSeleccionado.Name == anu.Name)
                    logs.AddRange(anu.Logs);
            }
            frlogs.Initialize(logs);
            frlogs.ShowDialog();
        }

        private void tsbLogsAutorenueva_Click(object sender, EventArgs e)
        {
            Entities.Anuncio anuncioSeleccionado = SelectedAdAutorenueva;
            if (SelectedAdAutorenueva == null)
                return;
            FrLogs frlogs = new FrLogs();
            List<Entities.Log> logs = new List<Entities.Log>();
            foreach (Entities.Anuncio anu in _anunciosJuntadoAutorenueva)
            {
                if (anuncioSeleccionado.Mail == anu.Mail)
                    logs.AddRange(anu.Logs);
            }
            frlogs.Initialize(logs);
            frlogs.ShowDialog();
        }

        private void stbStopAutorenueva_Click(object sender, EventArgs e)
        {
            updateCkecksAutorenueva(false);
        }

        private void stbPlayAutorenueva_Click(object sender, EventArgs e)
        {
            updateCkecksAutorenueva(true);
        }

        private void tsbRecargarSaldo_Click(object sender, EventArgs e)
        {

            if (olvAnunciosAutorenueva.SelectedObject == null)
            {
                MessageBox.Show("debe seleccionar una publicacion");
                return;
            }

            Entities.Anuncio selectedAnuncio = (Entities.Anuncio)olvAnunciosAutorenueva.SelectedObject;
            /*Browser browser = new IE(true);
            string url = "http://www.milanuncios.com/mis-anuncios/?recargar=1";
            getPagina(true, browser, url);
            Image ImageTarjeta = FindImage(browser, "tarjetas.png");
            if (ImageTarjeta != null)
            {   //cerramos la sesion abierta
                clickEnLink(browser, "javascript:document.location='/'+'mis-anuncios'+'/'");// para ir a mis anuncios
                Image salir = FindImage(browser, "icoSalir.gif");
                if (salir != null)
                {
                    salir.Click();
                    browser.WaitForComplete();
                }
                browser.Close();
                browser = new IE(true);
                getPagina(true, browser, url);
            }
            SetLoginSimple(selectedAnuncio.Mail, selectedAnuncio.Password, browser);
            Submit(browser);*/

            System.Diagnostics.Process proceso = System.Diagnostics.Process.Start("chrome.exe", "www.milanuncios.com/mis-anuncios/?recargar=1");
            InteractivePause(7);
            setLoginTabs(selectedAnuncio);
        }

        private void clickEnLink(Browser browser, String nombre)
        {
            Link anuncios = FindHref(browser, nombre);
            anuncios.Click();
            browser.WaitForComplete();
        }
        private void tsbModificarAnuncio_Click(object sender, EventArgs e)
        {
            if (olvAnunciosAutorenueva.SelectedObject == null)
            {
                MessageBox.Show("debe seleccionar una publicacion");
                return;
            }
            Entities.Anuncio selectedAnuncio = (Entities.Anuncio)olvAnunciosAutorenueva.SelectedObject;
            /*Browser browser = new IE(true);
            getPagina(true, browser, "http://www.milanuncios.com/mis-anuncios/");
            SetLogin(selectedAnuncio.Mail, selectedAnuncio.Password, browser);
            Submit(browser);
            */
            //intentos para abrir con otro navegador
            System.Diagnostics.Process proceso = System.Diagnostics.Process.Start("chrome.exe", "www.milanuncios.com/mis-anuncios/");
            InteractivePause(7);
            setLoginTabs(selectedAnuncio);
        }

        private void setLoginTabs(Entities.Anuncio selectedAnuncio)
        {
            //if (logueado())
            escribir(selectedAnuncio.Mail);
            hacerTap(1);
            escribir(selectedAnuncio.Password);
            hacerTap(2);
            //hacerSpace(1);
            //hacerTap(1);
            enter();
        }
        private void hacerSpace(int cantidad) {
            for (int i = 0; i < cantidad; i++)
            {
                System.Windows.Forms.SendKeys.SendWait("{SPACE}");
                InteractivePause(1);
            }
        }
        private bool logueado()
        {

            return !existeEsteTexto("Accede a tu cuenta para gestionar tus anuncios");
        }
        private bool existeEsteTexto(String texto)
        {
            return true;
        }

        public List<Entities.Anuncio> llenarHorasSalidas(List<Entities.Anuncio> anuncios)
        {
            int intervaloEntreAnuncios = 2;
            int intervalo = intervaloEntreAnuncios;
            List<Entities.Anuncio> res = anuncios;//.OrderBy(anuncio => anuncio.Mail).ToList();
            List<Entities.Anuncio> anunciosEnComun = new List<Entities.Anuncio>();
            //Entities.Anuncio anuncioAux = new Entities.Anuncio();
            string name = res[0].Name;
            foreach (Entities.Anuncio anu in res)
            {
                if (name == anu.Name)
                    anunciosEnComun.Add(anu);
                else
                {
                    try
                    {
                        generarHorariosMail(anunciosEnComun, intervalo);
                        intervalo += intervaloEntreAnuncios;
                    }
                    catch (Exception ex)
                    {

                    }
                    name = anu.Mail;
                    anunciosEnComun = new List<Entities.Anuncio>();
                    anunciosEnComun.Add(anu);
                }
            }
            return res;
        }
        public void generarHorariosDespuesDeRun(Entities.Anuncio anuncio)
        {
            List<Entities.Anuncio> anunciosEnComun = new List<Entities.Anuncio>();
            foreach (Entities.Anuncio anu in _anuncios)
            {
                if (anuncio.Name == anu.Name)
                {
                    anu.horasDeSalida = null;
                    anunciosEnComun.Add(anu);
                }
            }
            int cantidad = anunciosEnComun.Count;//10
            DateTime date = DateTime.Now.AddMinutes(1);
            double intervalo = ((int)anunciosEnComun[0].TimeInterval) / anunciosEnComun.Count;

            for (int i = 0; i < cantidad; i++)
            {
                foreach (Entities.Anuncio anu in anunciosEnComun)
                {
                    anu.horasDeSalida.Add(date);
                    date = date.AddMinutes(intervalo);
                }
            }
        }
        public void generarHorariosMail(List<Entities.Anuncio> anuncios, int interv)
        {
            int cantidad = anuncios.Count * 1000;//10
            DateTime date = DateTime.Now.AddMinutes(1).AddMinutes(interv);
            double intervalo = ((int)anuncios[0].TimeInterval) / anuncios.Count;

            for (int i = 0; i < cantidad; i++)
            {
                foreach (Entities.Anuncio anu in anuncios)
                {
                    anu.horasDeSalida.Add(date);
                    date = date.AddMinutes(intervalo);
                }
            }
        }
        
    }
}