﻿using System;
using System.Collections.Generic;
using System.Linq;
namespace AdtomaticLite.Entities
{
    public class Log
    {
        public DateTime FechaProgramada { get; set; }
        public DateTime FechaInicio { get; set; }
        public DateTime FechaFin { get; set; }
        public int CodigoResultado { get; set; }
        public string DescResult { get; set; }
    }
}
