﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AdtomaticLite.Entities
{
    public class Enum
    {
        public enum eTablones
        {
            MilAnuncioRenov,
            MilAnunciosRenovPago
        }

        public enum eRunType
        { 
            OneTime,
            Diary,
            Weekly,
            Monthly
        }
    }
}
