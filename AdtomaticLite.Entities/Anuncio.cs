﻿using System;
using System.Collections.Generic;
using System.Linq;
//using System.Windows.Forms.CheckBox;
using System.Windows.Forms;

namespace AdtomaticLite.Entities
{
    public class Anuncio
    {

        public Anuncio()
        {
            this.Logs = new List<Log>();
            this.activo = "inactivo";
            this.horasDeSalida = new List<DateTime>();
            this.punteroHoraSalida = 0;
        }
        public List<DateTime> horasDeSalida { get; set; }
        public string activo { get; set; }
        public int punteroHoraSalida { get; set; }
        public string Name { get; set; }
        public string Mail { get; set; }
        public string Password { get; set; }
        public Enum.eTablones Tipo { get; set; }
        
        public int IdRunType { get; set; }
        public string Saldo
        {
            get;
            set;
        }
        public bool check { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? StartTime { get; set; }
        public DateTime? EndDate { get; set; }
        public DateTime? EndTime { get; set; }
        public int? TimeInterval { get; set; }
        public bool? RunEveryDay { get; set; }
        public bool? RunWeekDays { get; set; }
        public DateTime? NextRun { get; set; }
        public bool IsMultipleTime
        {
            get { return (this.EndTime != null && this.TimeInterval != null && this.TimeInterval > 0); }
        }
        public DateTime? LastRun { get; set; }
        public int? RunEveryXDays { get; set; }
        public int? IntervaloRenovPago { get; set; }
        public int? RenovarNocturno { get; set; }
        public TimeSpan? startDefault { get; set; }
        public TimeSpan? endDefault { get; set; }
        public List<Log> Logs { get; set; }
        public string DescLastRun
        {
            get
            {
                if (this.Logs.Count == 0)
                    return string.Empty;
                return Logs.OrderByDescending(l => l.FechaFin).First().DescResult;
            }
        }
        public DateTime? NextRun2 
        { 
            get
            {
                if (this.Tipo == Enum.eTablones.MilAnunciosRenovPago)
                    return null;
                else
                    return this.NextRun;
            } 
        }

        public DateTime? CalculateNextRunDate()
        {
            DateTime? wNextRunDate = DateTime.Now;
            switch (this.IdRunType)
            {
                case (int)AdtomaticLite.Entities.Enum.eRunType.OneTime:
                    wNextRunDate = CalculateOneTime();
                    break;
                case (int)AdtomaticLite.Entities.Enum.eRunType.Diary:
                    wNextRunDate = CalculateNextDaily();
                    break;
                case (int)AdtomaticLite.Entities.Enum.eRunType.Weekly:
                    wNextRunDate = CalculateNextWeekly();
                    break;
                case (int)AdtomaticLite.Entities.Enum.eRunType.Monthly:
                    wNextRunDate = CalculateNextMonthly();
                    break;
                default:
                    throw new Exception("Incorrect Schedule Type");
            }
            return wNextRunDate;
        }

        private DateTime? CalculateOneTime()
        {
            try
            {
                if (this.StartDate == null || this.StartTime == null)
                    return null;

                if (this.EndTime != null && (this.TimeInterval == null || this.TimeInterval < 1))
                    return null;

                DateTime? wDate = DateTime.Now;
                DateTime wStartDateTime = new DateTime(this.StartDate.Value.Year, this.StartDate.Value.Month, this.StartDate.Value.Day, this.StartTime.Value.Hour, this.StartTime.Value.Minute, 0);
                if (this.LastRun != null)
                    wStartDateTime = GetDateTimeFS(this.LastRun.Value);

                if (wStartDateTime > wDate)
                {
                    wDate = wStartDateTime;
                }
                else
                {
                    if (this.IsMultipleTime)
                    {
                        DateTime wEndDateTime = new DateTime(this.StartDate.Value.Year, this.StartDate.Value.Month, this.StartDate.Value.Day, this.EndTime.Value.Hour, this.EndTime.Value.Minute, 0);

                        while (wStartDateTime < wDate)
                        {
                            try
                            {
                                wStartDateTime = wStartDateTime.AddMinutes((int)this.TimeInterval);
                            }
                            catch { break; }
                        }
                        if (wStartDateTime > wEndDateTime)
                            wDate = null;
                        else
                            wDate = wStartDateTime;
                    }
                    else
                    {
                        wDate = null;
                    }

                }

                return wDate;

            }
            catch
            {
                return DateTime.MaxValue;
            }
        }

        private DateTime? CalculateNextDaily()
        {
            try
            {
                if (this.StartDate == null || this.StartTime == null)
                    return null;

                if (this.EndTime != null && (this.TimeInterval == null || this.TimeInterval < 1))
                    return null;

                if (this.EndDate != null)
                {
                    DateTime wEndDateTime = DateTime.Now;
                    if (this.IsMultipleTime)
                        wEndDateTime = new DateTime(this.EndDate.Value.Year, this.EndDate.Value.Month, this.EndDate.Value.Day, this.EndTime.Value.Hour, this.EndTime.Value.Minute, 0);
                    else
                        wEndDateTime = new DateTime(this.EndDate.Value.Year, this.EndDate.Value.Month, this.EndDate.Value.Day, this.StartTime.Value.Hour, this.StartTime.Value.Minute, 0);

                    if (wEndDateTime < DateTime.Now)
                        return null;
                }

                DateTime? wDate = DateTime.Now;
                //DateTime? wDateNowStartTime = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, this.StartTime.Value.Hour, this.StartTime.Value.Minute, 0);//DateTime.Now;
                //if (wDateNowStartTime > wDate)
                //    wDate = wDateNowStartTime;
                
                DateTime wStartDateTime = new DateTime(this.StartDate.Value.Year, this.StartDate.Value.Month, this.StartDate.Value.Day, this.StartTime.Value.Hour, this.StartTime.Value.Minute, 0);
                if (this.LastRun != null)
                    wStartDateTime = GetDateTimeFS(this.LastRun.Value);
                else if (this.Tipo == Enum.eTablones.MilAnunciosRenovPago && this.IntervaloRenovPago != 0)
                    return this.NextRun;

                if (this.RunEveryDay != null && this.RunEveryDay == true)
                {
                    if (wStartDateTime > wDate)
                    {
                        wDate = wStartDateTime;
                    }
                    else
                    {
                        if (this.IsMultipleTime)
                        {

                            DateTime wStartDate = wStartDateTime;
                            DateTime wEndDate = new DateTime(wStartDate.Year, wStartDate.Month, wStartDate.Day, this.EndTime.Value.Hour, this.EndTime.Value.Minute, 0);

                            while (true)
                            {
                                while (wStartDate < wEndDate)
                                {
                                    if (wStartDate >= wDate)
                                        break;
                                    wStartDate = wStartDate.AddMinutes((int)this.TimeInterval);
                                }
                                if (wStartDate >= wEndDate)
                                {
                                    wStartDate = wStartDateTime.AddDays(1);//wStartDate
                                    //wStartDate = new DateTime(wStartDate.Year, wStartDate.Month, wStartDate.Day, this.StartTime.Value.Hour, this.StartTime.Value.Minute, 0);
                                    wStartDate = new DateTime(wStartDate.Year, wStartDate.Month, wStartDate.Day, this.StartTime.Value.Hour, this.StartTime.Value.Minute, 0);
                                    try
                                    {
                                        wEndDate = wEndDate.AddDays(1);
                                    }
                                    catch { break; }
                                    //wDate = null;
                                }
                                else
                                {
                                    wDate = wStartDate;
                                    break;
                                }
                            }
                        }
                        else
                        {
                            wStartDateTime = new DateTime(wStartDateTime.Year, wStartDateTime.Month, wStartDateTime.Day, this.StartTime.Value.Hour, this.StartTime.Value.Minute, 0);
                            while (wStartDateTime < DateTime.Now)
                            {
                                wStartDateTime = wStartDateTime.AddDays(1);
                            }
                            wDate = wStartDateTime;
                        }
                    }
                }
                else if (this.RunWeekDays != null && this.RunWeekDays == true)
                {
                    if (wStartDateTime > wDate)
                    {
                        wDate = wStartDateTime;
                    }
                    else
                    {
                        while (!(wStartDateTime > wDate))
                        {
                            wStartDateTime = wStartDateTime.AddDays(1);
                        }
                        wDate = wStartDateTime;
                    }
                    while (!(wDate.Value.DayOfWeek != DayOfWeek.Saturday & wDate.Value.DayOfWeek != DayOfWeek.Sunday))
                    {
                        wDate = wDate.Value.AddDays(1);
                    }
                }
                else if (this.RunEveryXDays > 0)
                {
                    if (wStartDateTime >= wDate)
                    {
                        wDate = wStartDateTime;
                    }
                    else
                    {
                        while (!(wStartDateTime >= wDate))
                        {
                            wStartDateTime = wStartDateTime.AddDays((int)this.RunEveryXDays);
                        }
                        wDate = wStartDateTime;
                    }
                }
                else
                {
                    wDate = DateTime.MaxValue;
                    //Para que no se ejecute nunca
                }

                return wDate;
            }
            catch
            {
                return DateTime.MaxValue;
            }

        }

        private DateTime? CalculateNextWeekly()
        {
            if (true)
                return null;

            DateTime wDate = DateTime.Now;
            DateTime wStartDateTime = (DateTime)this.StartDate;//new System.DateTime(mStartDate.Year, mStartDate.Month, mStartDate.Day, mStartTime.Hour, mStartTime.Minute, mStartTime.Second);

            /*if (this.RunEveryXWeeks > 0)
            {
                // Obtengo la primera fecha mayor a hoy, cada x semanas desde startdate
                while (!(wStartDateTime >= wDate))
                {
                    wStartDateTime = wStartDateTime.AddDays((int)this.RunEveryXWeeks * 7);
                }

                // Obtengo el primer dia que se debe ejecutar
                DateTime wDateAux = wStartDateTime;
                while (!(GetWeekNumber(wDateAux) != GetWeekNumber(wStartDateTime) || GetRunDay(wDateAux.DayOfWeek)))
                {
                    wDateAux = wDateAux.AddDays(1);
                }
                wDate = wDateAux;
            }
            else
            {
                wDate = DateTime.MaxValue;
                //Para que no se ejecute nunca
            }*/

            return wDate;

        }

        private System.DateTime CalculateNextMonthly()
        {
            DateTime wDate = DateTime.Now;
            DateTime wStartDateTime = (DateTime)this.StartDate; //new System.DateTime(mStartDate.Year, mStartDate.Month, mStartDate.Day, mStartTime.Hour, mStartTime.Minute, mStartTime.Second);

            /*//Si hay algo mal cargado devuelvo la fecha maxima
            if (this.RunDay == 0 & (this.RunDayType == 0 | this.RunDayDay == 0))
            {
                wDate = DateTime.MaxValue;
                //Para que no se ejecute nunca
                return wDate;
            }

            // Obtengo la fecha de comienzo (hoy o startdatetime)
            DateTime wDateAux = Convert.ToDateTime((wStartDateTime > wDate ? wStartDateTime : wDate));

            do
            {
                if (GetRunMonth(wDateAux.Month - 1))
                {
                    if (this.RunDay > 0)
                    {
                        if (this.RunDay >= wDateAux.Day)
                        {
                            int wDay = Convert.ToInt32((this.RunDay > DateTime.DaysInMonth(wDateAux.Year, wDateAux.Month) ? DateTime.DaysInMonth(wDateAux.Year, wDateAux.Month) : this.RunDay));
                            wDate = new System.DateTime(wDateAux.Year, wDateAux.Month, wDay, wDateAux.Hour, wDateAux.Minute, wDateAux.Second);
                            break; // TODO: might not be correct. Was : Exit Do
                        }
                    }
                    else
                    {
                        System.DateTime wDateA = GetDateSpecial(wDateAux.Year, wDateAux.Month, mRunDay_Type, mRunDay_Day);
                        if (wDateA >= wDateAux)
                        {
                            wDate = new System.DateTime(wDateA.Year, wDateA.Month, wDateA.Day, wDateAux.Hour, wDateAux.Minute, wDateAux.Second);
                            break; // TODO: might not be correct. Was : Exit Do
                        }
                    }
                }
                // paso al siguiente mes
                wDateAux = wDateAux.AddMonths(1);
                wDateAux = new System.DateTime(wDateAux.Year, wDateAux.Month, 1, wDateAux.Hour, wDateAux.Minute, wDateAux.Second);
            } while (true);
            */
            return wDate;

        }

        private DateTime GetDateTimeFS(DateTime dt)
        {
            return new DateTime(dt.Year, dt.Month, dt.Day, dt.Hour, dt.Minute, 0);
        }
        public DateTime? nextRunNuevo() {
            if (horasDeSalida.Count != 0)
                return horasDeSalida[punteroHoraSalida];
            else
                return null;
        }
        public void aumentarPuntero(){
            punteroHoraSalida++;
        }
        public Entities.Anuncio Clone()
        {
            Entities.Anuncio anuncio = new Entities.Anuncio();
            anuncio.Name = this.Name;
            anuncio.Mail = this.Mail;
            anuncio.Password = this.Password;
            anuncio.Tipo = this.Tipo;
            anuncio.IdRunType = this.IdRunType;
            anuncio.StartDate = this.StartDate;
            anuncio.StartTime = this.StartTime;
            anuncio.EndDate = this.EndDate;
            anuncio.EndTime = this.EndTime;
            anuncio.TimeInterval = this.TimeInterval;
            anuncio.RunEveryDay = this.RunEveryDay;
            anuncio.RunWeekDays = this.RunWeekDays;
            anuncio.NextRun = this.NextRun;
            anuncio.LastRun = this.LastRun;
            anuncio.RunEveryXDays = this.RunEveryXDays;
            anuncio.IntervaloRenovPago = this.IntervaloRenovPago;
            anuncio.RenovarNocturno = this.RenovarNocturno;
            anuncio.Saldo = this.Saldo;
            anuncio.check = this.check;
            anuncio.activo = this.activo;
            anuncio.startDefault = this.startDefault;
            anuncio.endDefault = this.endDefault;
            anuncio.horasDeSalida = this.horasDeSalida;
            anuncio.punteroHoraSalida = this.punteroHoraSalida;
            // TODO: Clonar logs?
            return anuncio;
        }
    }
}
